
# README #

THE CURRENT VERSION OF THE FRAMEWORK 1.5.20180807 (former 3.45)


## SUPPORTED BROWSERS (TESTED): 

**Firefox 52_7_3+**
**Firefox 57.0.1 (Quantum)**

**Chrome 67.0.3396.99**

**IE 9** works in general but in some cases with workarounds (e.g. "click"-image on feedbackoverlays)  **IE 10,11** on Windows 7

**Safari** -

## FOREWORD:

The framework is supposed to expedite our workflow to keep up with the development times for the interactives. Therefore i implemented some features that i want to highlight and explain here. Of course i can't cover everything here, so please forgive me if something is missing. I will cover the main topics:

 - **how to work with the framework**
 - **folder structure**
 - **general logic**
	 - globalVar object
	 - framework logic
	 - 
 - **data.js** structure
	 - objects
	 - types and their purpose
 - **index.html** structure
	 - header
	 - body
		 - slides/sections
		 - feedbacks
	 - menu
	 - charts
		 - screen_chart 
		 - toc charts
	 - 
 - css adjustments


Reveal.initialize

element.css

zindex smartinjs 
 
### How to work with the framework:

#### Setup
This is the overview of all core files that you need for the framework:
Important files are highlighted in grey with a hashtag in front (sorry, bitbucket markdown works weird ;) ).

```
#!shell

├── css
│   ├── _css
│   │   ├── elements.css
│   │   ├── fonts
│   │   │   ├── interactive.eot
│   │   │   ├── interactive.svg
│   │   │   ├── interactive.ttf
│   │   │   └── interactive.woff
│   │   ├── icons.css
│   │   ├── interactive.css
│   │   ├── reveal.css
│   │   ├── theme
│   │   │   └── white.css
│   │   └── videojs
│   │       ├── font
│   │       │   ├── VideoJS.eot
│   │       │   ├── VideoJS.svg
│   │       │   ├── VideoJS.ttf
│   │       │   ├── VideoJS.woff
│   │       │   ├── VideoJSInteractive.eot
│   │       │   ├── VideoJSInteractive.svg
│   │       │   ├── VideoJSInteractive.ttf
│   │       │   └── VideoJSInteractive.woff
│   │       ├── fonts.css
│   │       ├── interactive-new.css
│   │       ├── interactive.css
│   │       ├── video-js.min.css
│   │       └── videojs-resolution-switcher.min.css
│   ├── # interactive.setup.css
│   ├── # navigation.css
│   ├── # sidebar.css
│   └── videojs
│       └── #colors.css
├── # imsmanifest.xml
├── # index.html
├── js
│   ├── _js
│   │   ├── SCORM_API_wrapper.min.js
│   │   ├── chart.js
│   │   ├── classList.js
│   │   ├── head.min.js
│   │   ├── interactive.scorm.js
│   │   ├── jquery-3.2.1.min.js
│   │   ├── jquery-ui-touchpunch.js
│   │   ├── jquery-ui.min.js
│   │   ├── polyfill_typedarray.js
│   │   ├── progressbar.min.js
│   │   ├── reveal.js
│   │   ├── smartin.js
│   │   └── videojs
│   │       ├── video.min.js
│   │       ├── videojs-resolution-switcher.min.js
│   │       └── videojs.hotkeys.min.js
│   ├── # data.js
│   └── # functions.js
└── media
    ├── audio
    │   ├── Q1
    │   │   ├── # 0.mp3
    │   │   ├── # FBPos.mp3
    │   │   ├── # FBNeg.mp3
    │   │   └── # FBNneg.mp3
    │   ├── Q2
    │   │   ├── # 0.mp3
    │   │   ├── # 1.mp3
    │   │   ├── # 2.mp3
    │   │   ├── # FBPos.mp3
    │   │   ├── # FBNeg.mp3
    │   │   └── # FBNneg.mp3
    │   └── sounds
    │       ├── MouseClick.mp3
    │       ├── doom.mp3
    │       ├── falsch.mp3
    │       └── korrekt.mp3
    ├── fonts
    │   ├── # PatrickHand-Regular.ttf
    │   ├── # YanoneKaffeesatz-Bold.ttf
    │   ├── # YanoneKaffeesatz-Light.ttf
    │   ├── # YanoneKaffeesatz-Regular.ttf
    │   ├── # YanoneKaffeesatz-Thin.ttf
    │   └── # simpleshow.ttf
    ├── # fonts.css
    ├── gifs
    │   └── # test.gif
    ├── imgs
    │   ├── # Artboard1.svg
    │   ├── # Asset1.svg
    │   ├── # Asset2.svg
    │   ├── Buttons
    │   │   ├── # check_button_clicked.svg
    │   │   ├── # check_button_down.svg
    │   │   ├── # check_button_over.svg
    │   │   ├── # check_button_up.svg
    │   │   ├── # replay_button_down.svg
    │   │   ├── # replay_button_over.svg
    │   │   ├── # replay_button_up.svg
    │   │   ├── # reset_button_down.svg
    │   │   ├── # reset_button_over.svg
    │   │   ├── # reset_button_up.svg
    │   │   ├── # start_button_down.svg
    │   │   ├── # start_button_over.svg
    │   │   ├── # start_button_up.svg
    │   │   ├── # submit_button_down.svg
    │   │   ├── # submit_button_over.svg
    │   │   └── # submit_button_up.svg
    │   ├── # IE9hack.svg
    │   ├── Icons
    │   │   ├── # icon_click_button.svg
    │   │   ├── # icon_drag_button_down.svg
    │   │   ├── # icon_drag_button_left.svg
    │   │   ├── # icon_drag_button_right.svg
    │   │   ├── # icon_drag_button_up.svg
    │   │   ├── # info_i_button_up.svg
    │   │   └── # info_plus_button_up.svg
    │   ├── # Info_Overlay.svg
    │   ├── _imgs
    │   │   ├── FBOverlay_white.svg
    │   │   └── clock.svg
    │   └── # logo.svg
    ├── pdfs
    │   └── # interactivelogo.pdf
    └── video
        └── 00_Intro
            ├── # poster.jpg
            ├── # subs.en.vtt
            ├── # video_1080.mp4
            ├── # video_360.mp4
            └── # video_720.mp4
```

###How to use drag and drops
IN GENERAL: (datajs/indexhtml)

```
quiz : 
  "dropTargets": {
    1 : ["dropSources"],
    2 : ["dropSources"],
    n : ["dropSources"]
  }
}
```

###### OR

```
quiz : 
  "dropTargets": {
    1 : ["dropSources"]
  }
}
```

#### Examples:
1 drop target - 6 drop sources (5 correct ones)

```
quiz:{
  drop1: {
    1:["drag1","drag2","drag4","drag5","drag6"]
  }
}
```

3 drop target - 5 drag sources (mulitple answers and drag sources per drop target)

```
quiz: {
  "LZ_4_Q1_drTarget1": {
    1 : ["LZ_4_Q1_drSource1", "LZ_4_Q1_drSource4"]
  }, 
  "LZ_4_Q1_drTarget2":{
    1 : ["LZ_4_Q1_drSource2", "LZ_4_Q1_drSource3"]				
  },
  "LZ_4_Q1_drTarget3": {
    1 : ["LZ_4_Q1_drSource5"]
  }
}
```

5 drop targets - 8 drag sources (on one drop target it is possible to drop 5 particular drag sources and it would be correct. The drop target of the drag source doensn't matter)

```
quiz: {
  drop1: {
    1:["drag1"],
    2:["drag2"],	
    3:["drag6"],	
    4:["drag7"],	
    5:["drag8"]		
  },
  drop2: {
    1:["drag1"],
    2:["drag2"],	
    3:["drag6"],	
    4:["drag7"],	
    5:["drag8"]		
  },
  drop3: {
    1:["drag1"],
    2:["drag2"],	
    3:["drag6"],	
    4:["drag7"],	
    5:["drag8"]		
  },
  drop4: {
    1:["drag1"],
    2:["drag2"],	
    3:["drag6"],	
    4:["drag7"],	
    5:["drag8"]		
  },
  drop5: {
    1:["drag1"],
    2:["drag2"],	
    3:["drag6"],	
    4:["drag7"],	
    5:["drag8"]		
  }
}
```

To create an action on a drag element, create the actions object inside of the dnd object: 

```
actions:{
  drag1:{
    onDragStart : function(){},
    onDrag : function(){},
    onDragOver : function(){},
    onDragEnd : function(){},
    onDrop : function(){}
  }
},
```
If you don't want to let happen anything, just comment the specific line or leave it out the code. 
**Be careful, the onDrop and onDragOver isn't specific for a particular drop target for now. It will execute the code that you write regardless of the drop target.**

You can reset Drag and Drops bei calling: ```resetDnd()``` But you must be on the same slide. You can't call this function when you are on a different slide with a different type. 

### Using the charts
##### Menu
Structure of a chapter element inside of menu:
```
<li id="Ch_4" data-slide="28" class="Menu_Nav Nav_DnD toc_clicker">Title Text
  <span id="Menu_Chart_4" class="spanchart chart" data-percent="0">
    <span class="percent"></span>
  </span>
</li>
```
If you want to enable / disable the progress-bar inside of the menu, **comment** or **uncomment** this part of the code : 

```
<span id="Menu_Chart_4" class="spanchart chart" data-percent="0">
  <span class="percent"></span>
</span>
```

You can animate a specific chart with (n - being your number):  ```charts["Menu_Chart_4"].animate(n);```
If you want to read the current value and add a new value and store it (n - being your number):
```
var x = parseFloat($("#Menu_Chart_4").attr("data-percent")) +  n;
charts["Menu_Chart_4"].animate(x);
$("#Menu_Chart_4").attr("data-percent", x);
```
You will jump to the slide that is inside of the data-slide attribute. 
```
<li id="Ch_4" data-slide="28" class="Menu_Nav Nav_DnD toc_clicker">
```
You can change the icon before the text with one of those classes:
```
Nav_DnD Nav_Quiz Nav_Video
```
To change the color/position adjust it to your project inside of the css code. Therefore go to the following files:
```
css/navigation.css 
css/sidebar.css
```

##### Course/On-Screen
Structure of the progress-bar on screen (bottom-right-corner):
```
<!-- data-name: depends on the language version, data-offset: the offset to the left when on 100%, depends on the width of the name (you can set an int-number)-->
<div id="Screen_Chart" class="chart post-94 fs-20 h-06 w-20" data-percent="0"  style="right: -7.3%;" data-name="abgschlossen" data-offset="0"></div>
```
You can animate the chart with (n - being your number):  ```charts["Screen_Chart"].animate(n);```

When you want to change the text next to the progress-bar, just change it inside of the data-name attribute: ```data-name="abgschlossen"``` and then change the  ```style="right: -7.3%;"```.


If you want to read the current value and add a new value and store it (n - being your number):
```
var x = parseFloat($("#Screen_Chart").attr("data-percent")) +  n;
charts["Screen_Chart"].animate(x);
$("#Screen_Chart").attr("data-percent", x);
```
Because the text next to the progress-bar will change it's position when you are on 100%, you can write a number into the data-offset, to work against this behaviour, change it: ```data-offset="0"```

If you want to update the progress-bar according to the course, go to ```js/Functions.js```
and find the ```chartProgress()``` and ```getPgr()```. Inside of the ```getPgr()``` you write a value between 0 and 1 in this line:
```
var x = parseFloat($("#Screen_Chart").attr("data-percent")) +  n;
```
If your course is not divisible exactly round by 100, then make a second getPgr2() and call it just once with another percentage. 

The function chartProgress() is called every slide change.

To change the color/position adjust it to your project inside of the css code. Therefore go to the following files:
```
css/navigation.css 
css/sidebar.css
```

Functions to call:

disableSubmit();
enableSubmit();

enableControls();
disableControls();



**MIKE Edits Begin**

####Comments
so i just downloaded the latest smartinjs template to start my latest project and i noticed that the data.js is pretty unorganized. all of the slides are structured in different orders. it makes sense to have all of the slides key value pairs in the same order to make life easier

the "type" and onEnter should be at the top of the slide
all of the "actions" should be in sequence. things that we don't really edit like visited should be at the bottom

##### Menu
I've written this code into all my menus. 

In the html, place your clickabale sections that jump to different sections of the course.
```
var $menuBoxes = $('.menu-box');
```

Place this into the data.js 
```
$menuBoxes.on('click touch', function(event){
					var $this = $(this);
					switch ($this[0].id) {
						case 'menu-box-1':
								jumpToSlide(3)
							break;
						case 'menu-box-2':
								jumpToSlide(6)
							break;
						case 'menu-box-3':
								jumpToSlide(9)
							break;
						case 'menu-box-4':
								jumpToSlide(14)
							break;
						default:
							break;
					}
				})
```


####Videos
The width and height in the HTML have no effect on the size. Changes must be made in CSS. To change the position, width and height of the videos you must change these CSS settings.
```
.video-resize {
    width: 1024px !important;
    top: 10%;
    left: 10%;
    padding: 288px !important;
}
```

This code enables the next button once a video has been seen.

Place this object in Functions JS with all your global variables.
```
var videoSeen = {
	V1: false,
	V2: false,	
	V3: false, 
	V4: false,
	V5: false
}
```
Place this code into your onEnterAction in data.js.
```
var $video = globalVar.$curSlide.find('video');
				if(videoSeen.V1) {
					nextShow();
				} else {
					nextHide();
				}

				$video.on('ended', function(){
					videoSeen.V1 = true;
				})
```

####Touch Screens
To prevent the default behavior of the click on a touch device, you have to write:
```
$finish.on("click touch", function () {
		if(event.type === 'click') {
		  event.stopPropagation();
		  event.preventDefault();
        }
		audPlayerMouseClick.play();
		nextSlide();
});	
```
This is useful when you have an interaction that only uses the keyboard and not the mouse on regular screens. 

**MIKE Edits End**

To access the videojs API when you are on a video slide use this piece of code: ```videojs(globalVar.$curSlide.find("video")[0].id)```