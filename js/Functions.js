//Chapter variables(stores the if question is correct answered/or the slide visited/...) --> works currently only with numbers 
//everytime you set a C_Qx variable to one, it will update the progress chart to the max amount of C_Qx variables (start with 1)
var C_Q1 = C_Q2 = C_Q3 = C_Q4 = C_Q5 = 0;
var curChapter;
var correctAnswers = 0;

function firstTimeSetup(){
	//first and only execution of code, when the course starts first time
}

function sendDataToLMS(){
	return correctAnswers;//add your additional variables here(string or variables, seperated by comma and only numbers are allowed)
}

function recieveDataFromLMS(){
	return "correctAnswers";//names of the variables that you send(see sendDatatoLMS)
}

function bookMarkingToLMS(){
	return curChapter;
}

function actionsOnEverySlideEnter(){
 //whatever you want to execute on every slide change
}

$(document).ready(function () {
	$("#downloadbutton").on("click", function () {
		window.open("media/pdfs/interactivelogo.pdf");
	});

	//when clicking on a TOC element
	$("#Menu_Side_Content li").on("click touch", function () {
		// console.log(this.id);
		// var num = this.id.substr(3, 1);

		// if (curChapter >= num) {
			setTimeout(function () {
				jumpToSlide($(temp).data("slide"));
			}, 500);
		// }
		TOC_Opener();

	});

	$(".clickable").on("click touch", function () {
		audPlayerMouseClick.play();
		Reveal.slide($(this).data("slide"));
	});

});


function developmentStuff() {
	if (slides[globalVar.curSlide] != undefined) {
		$("#dev_slides").empty();
		$("#dev_slides").append(
			slides[globalVar.curSlide].slideNumber + " / " + $("section").not(".testarea").length + "<br>" +  //number of slides
			globalVar.curSlide + "<br>" + //name of slide
			slides[globalVar.curSlide].type + "<br>" + //type of slide
			C_Q1 + " | " + C_Q2 + " | " + C_Q3 + " | " + C_Q4 + " | " + C_Q5 +"<br>" //chapter variables 
		);
	}
}

//Add here the functioniality what happens when click on FB Overlay
function FBClick(item){
	if (item == "FB_Pos") {
		//what should happen when click on FBPos
		slides[globalVar.curSlide].nextAction();
	}
	if (item == "FB_Neg") {
		//what should happen when click on FBNeg
			// slides[globalVar.curSlide].nextAction();			
			if(slides[globalVar.curSlide][slides[globalVar.curSlide].type].userAttempts == 1){
				slides[globalVar.curSlide].nextAction();
			}			
	}
	if (item == "FB_Nneg") {
		//what should happen when click on FBNneg
			slides[globalVar.curSlide].nextAction();
	}
}

//the holy cheatfunction!!! 
function idbehold() {
	// Cheat code. See Doom.
	globalVar.idbehold_on = 1;
	$("body").css("background-color", "red");
	console.log("GOD MODE!");
	$("#guidelines").show();

	$("#scorm_whiteOverlay").hide();
	datajsCheck();

	$( document ).keyup(function(e) {
		// console.log(e.which);
	// });

	// document.onkeyup=function(e){
		// var e = e || window.event; // for IE to cover IEs window event-object
		if(/*e.shiftKey && */e.which == 71) {
			if($("#guidelines").css("display")!="none"){
				$("#guidelines").hide();
			}else{
				$("#guidelines").show();
			}
			// alert(Keyboard shortcut working!);
			// return false;

		}

		if(/*e.shiftKey && */e.which == 74) {
			$("#"+globalVar.curSlide).find(".FB_Pos").show();
			$("#"+globalVar.curSlide).find(".FB_Neg").hide();
			$("#"+globalVar.curSlide).find(".FB_Nneg").hide();
			// alert(Keyboard shortcut working!);
			// return false;
		}

		if(/*e.shiftKey && */e.which == 37) {
			// console.log("hausdh");
			prevSlide();
			
		}
		if(/*e.shiftKey && */e.which == 39) {
			// nextSlide();
			Reveal.next();
		}

		if(/*e.shiftKey && */e.which == 75) {
			$("#"+globalVar.curSlide).find(".FB_Pos").hide();
			$("#"+globalVar.curSlide).find(".FB_Neg").show();
			$("#"+globalVar.curSlide).find(".FB_Nneg").hide();
			// alert(Keyboard shortcut working!);
			// return false;
		}

		if(/*e.shiftKey && */e.which == 76) {
			$("#"+globalVar.curSlide).find(".FB_Pos").hide();
			$("#"+globalVar.curSlide).find(".FB_Neg").hide();
			$("#"+globalVar.curSlide).find(".FB_Nneg").show();
			// alert(Keyboard shortcut working!);
			// return false;
		}

	if(/*e.shiftKey && */e.which == 186) {
		$("#"+globalVar.curSlide).find(".FB_Pos").hide();
		$("#"+globalVar.curSlide).find(".FB_Neg").hide();
		$("#"+globalVar.curSlide).find(".FB_Nneg").hide();
		// alert(Keyboard shortcut working!);
		// return false;
		}
	});
}