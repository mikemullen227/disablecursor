var charts={};
function chartInitiate(){
	// if(_toc!=false){
// 		var bar = new ProgressBar.Circle(Screen_Chart, {
// 			strokeWidth: 4,
// 			easing: 'easeInOut',
// 			duration: 1400,
// 			color: '#0A1E6E',
// 			trailColor: '#0A1E6E',
// 			trailWidth: 1,
// 			svgStyle: null,
// 				svgStyle: {width: '100%', height: '100%', margin:'1%'},
// 		text: {
// 			style: {
// 				// Text color.
// 				// Default: same as stroke color (options.color)
// 	//      color: '#fff',
// 	//      position: 'absolute',
// 	//      right: '210',
// 	//      top: '0px',
// 	//      padding: 0,
// 	//      margin: '1%',
// 				transform: null
// 			},
// 			autoStyleContainer: false
// 		},
// 			step: function(state, bar){
// 				return bar.setText(Math.round(bar.value()*100) + '% complete');
// 			}
// //		step: (state, bar) => {
// //			bar.setText(Math.round(bar.value() * 100) + ' % complete');
// //		}
// 		});
// 		charts["Screen_Chart"] = bar;
	
// 		var bar = new ProgressBar.Circle(Menu_Chart_Big, {
// 			strokeWidth: 2,
// 			easing: 'easeInOut',
// 			duration: 1400,
// 			color: '#0A1E6E',
// 			trailColor: '#fff',
// 			trailWidth: 7,
// 			svgStyle: null,		text: {
// 				style: {
// 					// Text color.
// 					// Default: same as stroke color (options.color)
// 		//      color: '#fff',
// 		//      position: 'absolute',
// 		//      right: '210',
// 		//      top: '0px',
// 		//      padding: 0,
// 		//      margin: '1%',
// 					transform: null
// 				},
// 				autoStyleContainer: false
// 			},
// 				step: function(state, bar){
// 					return bar.setText(Math.round(bar.value()*100) + '% complete');
// 				}
// 	//		step: (state, bar) => {
// 	//			bar.setText(Math.round(bar.value() * 100) + ' % complete');
// 	//		}
// 			});

// 		// console.log(bar);
		

// 		bar.animate(0);  // Number from 0.0 to 1.0
// 		charts["Menu_Chart_Big"] = bar;
	
		$(".spanchart").each(function (i) {
			var bar = new ProgressBar.Circle(document.getElementById(this.id), {
				strokeWidth: 2,
				easing: 'easeInOut',
				duration: 1400,
				color: '#0A1E6E',
				trailColor: '#fff',
				trailWidth: 6,
				svgStyle: null
			});
			bar.animate(0);  // Number from 0.0 to 1.0
			charts[this.id] = bar;
		});

		var bar = new ProgressBar.Circle(Screen_Chart, {
			strokeWidth: 4,
			easing: 'easeInOut',
			duration: 1400,
			color: '#000',
			trailColor: '#0079DA',
			trailWidth: 1,
			svgStyle: null,
			svgStyle: {width: '15%', height: '100%'},
			text: {
				style: {
					transform: null
				},
				autoStyleContainer: false
			},
			step: function(state, bar){
				if (Math.round(bar.value()*100) >= 100) {
					$(".progressbar-text").css("left", $("#Screen_Chart").data("offset")+"%");
				}
				return bar.setText(Math.round(bar.value()*100) + '% '+ $("#Screen_Chart").data("name"));
			}
		});
		charts["Screen_Chart"] = bar;
	// }
}