var scorm = pipwerks.SCORM; //Shortcut
var lmsConnected = false;
var unloaded = false;

function handleError(msg) {
	console.log(msg);
	//window.close();
}

function initCourse() {
	//scorm.init returns a boolean
	lmsConnected = scorm.init();
	//If the scorm.init function succeeded...
	if (lmsConnected) {
		//Let's get the completion status to see if the course has already been completed
		var completionstatus = scorm.get("cmi.core.lesson_status");
		//If the course has already been completed...
		if (completionstatus === "completed" || completionstatus === "passed") {
			//...let's display a message and close the browser window
			handleError("Info: You have already completed this course.");
		}
		//Now let's get the username from the LMS
		var learnername = scorm.get("cmi.core.student_name");
		//If the name was successfully retrieved...
		if (learnername) {
			//...let's display the username in a page element named "learnername"
			//document.getElementById("learnername").innerHTML = learnername; //use the name in the form
		}
		//If the course couldn't connect to the LMS for some reason...
	}
	else {
		//... let's alert the user then close the window.
		handleError("Error: Course could not connect with the LMS");
	}
}

function setComplete() {
	//If the lmsConnection is active...
	if (lmsConnected) {
		//... try setting the course status to "completed"
		var success = scorm.set("cmi.core.lesson_status", "completed");
		//If the course was successfully set to "completed"...
		if (success) {
			//... disconnect from the LMS, we don't need to do anything else.
			scorm.quit();
			handleError("Info: Course was successfully set to complete!");
			//If the course couldn't be set to completed for some reason...
		}
		else {
			//alert the user and close the course window
			handleError("Error: Course could not be set to complete!");
		}
		//If the course isn't connected to the LMS for some reason...
	}
	else {
		//alert the user and close the course window
		handleError("Error: Course is not connected to the LMS");
	}
}

function unloadHandler(){

	scorm.set("cmi.core.score.min",globalVar.minScore);
	scorm.set("cmi.core.score.max",globalVar.maxScore);

	smar_sendDataToLMS();
   if(!unloaded){
      scorm.save(); //save all data that has already been sent
      scorm.quit(); //close the SCORM API connection properly
      unloaded = true;
   }
}

window.onload = function () {
	if (globalVar.scormSettings.useScorm) {
		console.log("SCORM mode");
		initCourse();
		window.onbeforeunload = unloadHandler;
		window.onunload = unloadHandler;
	} else {
		console.log("SCORM off");
	}
}
