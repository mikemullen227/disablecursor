#Too Many Feedbacks!!#

This script allows you to have a feedback slide for each question, plus an additional one that shows the correct answer on the 2nd wrong attempt. 
In this case there are 4 questions, each with a radio button, and each one has its own corresponding feedback slide. 1 positive feedback slide(1st or 2nd correct attempt), 
3 negative(1st wrong attempt) and 1 that shows the correct answer (2nd wrong attempt). It was implemented for a scq but could work with an mcq with some tweaking. 


###HTML
```
 radio button with a data-feedback that has the id of the corresponding feedback 
	<div id="Q1B1" class="si_Button_2 post-74 posl-12 pos-abs w-05 h-08 elz-4" data-checked="false" data-feedback="fn1">
		<img class="button_up" src="media/imgs/Buttons/radio_button_up.svg">
		<img class="button_over" src="media/imgs/Buttons/radio_button_over.svg" style="display: none;">
		<img class="button_down" src="media/imgs/Buttons/radio_button_down.svg" style="display: none;">
	</div>
	
 corresponding feedback slide 
	<div id="fn1" class="FB_Neg1 feedbacks elz-5 ">
		<img class="ie9hack" src="media/imgs/IE9hack.svg">
		<img class="post-00 posl-00 elz-6" src="media/imgs/01_Q1/q1_fn1.svg">
	</div>
	
 submit button 
	<div id="submit1" class="my_Button disabledBtn post-90 posl-85 elz-4"></div>
	
```


###JS

```

onEnterAction: function () {
				// store radio buttons, feedback slides, and submit button in jQuery objects
				var $buttons = $(".si_Button_2");
				var $fbNeg4 = $("#fn4");
				var $feedbacks = $(".feedbacks");
				var $q1Submit = $("#submit1");
				var attempts = 0;
				var $currentFB;
				var fBackID;

				// reset all the buttons on slide enter. submit button is disabled. 
				$buttons.data("checked", false);
				$buttons.off("click");
				$q1Submit.addClass("disabledBtn");
				// hide all feedbacks on enter
				$feedbacks.off();
				$feedbacks.hide();
				$q1Submit.off();

				//function to run after however many attempts you've decided. resets all the radio buttons to original setting in case of re-entry 
				//to slide
				function slideExit() {
					$buttons.each(function() {
						$this = $(this);
						$this.children().eq(0).show();
						$this.children().eq(1).hide();
						$this.children().eq(2).hide();
					})
					Q_1 = 1;
					// disableSubmit();
					nextSlide();
					
				}
				
				// hide show buttons depending on event type. looks at html data attribute to decide what to do.
				$buttons.on("mouseenter mouseleave click touch", function (event) {
					var $this = $(this);
					if (event.type === 'mouseenter' && $this.data("checked") === false) {
						$this.children().eq(0).hide();
						$this.children().eq(1).show();
					} else if (event.type === 'mouseleave' && $this.data("checked") === false) {
						$this.children().eq(0).show();
						$this.children().eq(1).hide();
					} else if (event.type === 'click' || event.type === 'touch' && $this.data("checked") === false) {
						//when clicking a button, resets all other buttons to original settings 
						audPlayerMouseClick.play();	
						$buttons.each(function() {
							var $that = $(this);
							 $that.data("checked", false);
							 $that.children().eq(1).hide();
							 $that.children().eq(2).hide();
							 $that.children().eq(0).show();
						});
						$this.children().eq(0).hide();
						$this.children().eq(1).hide();
						$this.children().eq(2).show();
						setTimeout(function() {
							$this.children().eq(0).hide();
							$this.children().eq(2).hide();
							$this.children().eq(1).show();
						}, 55)
						//changes data attribute to true once clicked. submit button is now clickable
						$this.data("checked", true);
						$q1Submit.removeClass("disabledBtn");
					} 
				});		
				
				
				$q1Submit.on('click touch', function() {
					// audPlayerMouseClick.play();	
					console.log(attempts);	
					// loop through each radio button to determine which one is clicked. get the data-id from the button
					$buttons.each(function() {
						var $this = $(this);
						if($this.data("checked") == true) {
							fBackID = $this.data("feedback");
						}
					})
					// filter out the corresponding feedback with the button by matching the id
					$currentFB = $feedbacks.filter(function() {
						return this.id === fBackID;
					});

					// if fBackID exists, than a button was clicked. test logic against amount of attempts and which feedback id was clicked. 

					if(fBackID){
						attempts++;
						if(attempts === 1 && fBackID === 'fn1') {
							audPlayerMouseClick.play();
							$currentFB.fadeIn().addClass("FB_cur");
							//each feedback has a corresponding voiceover. enter an argument that corresponds with its audio track
							wrongSound(1);
						} else if(attempts === 1 && fBackID === 'fn2') {
							audPlayerMouseClick.play();
							$currentFB.fadeIn().addClass("FB_cur");
							wrongSound(2);
						} else if(attempts === 1 && fBackID === 'fn3') {
							audPlayerMouseClick.play();
							$currentFB.fadeIn().addClass("FB_cur");
							wrongSound(3);
						} else if(attempts === 1 || attempts === 2 && fBackID === 'fp1') {
							audPlayerMouseClick.play();
							$currentFB.fadeIn().addClass("FB_cur");
							rightSound();
						} else {
							audPlayerMouseClick.play();
							$fbNeg4.fadeIn().addClass("FB_cur");;
							wrongSound(4);
						}
					}	
				})
				
				// when clicking feedbacks, determine whether to stay or leave slide
				$feedbacks.on('click touch', function() {
					var $this = $(this);
					if(fBackID === 'fp1') {
						$(audPlayerOne).trigger("pause");
						$this.hide().removeClass("FB_cur");; 
						slideExit();	
					} else if(attempts === 2) {
						$this.hide().removeClass("FB_cur");; 
						slideExit();
					} else {
						$(audPlayerOne).trigger("pause");
						$q1Submit.addClass("disabledBtn");
						$this.hide().removeClass("FB_cur");;
					}
				})
			}

```
###Voice overs that play with the corresponding feedback slide

Negative Feedbacks 
```
function wrongSound(n) {
	$(audPlayerOne).trigger("pause");
	audPlayerOne.src = slides[globalVar.curSlide].audio["fBNeg" + n];
	console.log(audPlayerOne.src)
	if (audUser == false) {
		audPlayerOne.play();
	}

	setTimeout(function () {
		var audPlayerFour = new Audio("");
		audPlayerFour.src = globalVar.wrongSound;
		audPlayerFour.volume = 0.5;

		if (audUser == false) {
			audPlayerFour.play();
		}
	}, 200);
}
```
Positive Feedback 
```
function rightSound() {
	$(audPlayerOne).trigger("pause");
	audPlayerOne.src = slides[globalVar.curSlide]["audio"]["FBPos"];
	if (audUser == false) {
		audPlayerOne.play();
	}

	setTimeout(function () {
	audPlayerThree.src = globalVar.correctSound;
	audPlayerThree.volume = 0.5;

	if (audUser == false) {
		audPlayerThree.play();
	}
}, 50);

	FBPos: "media/audio/Q1/FBp.mp3",
	fBNeg1: "media/audio/Q1/FBn1.mp3",
	fBNeg2: "media/audio/Q1/FBn2.mp3",
	fBNeg3: "media/audio/Q1/FBn3.mp3",
	fBNeg4: "media/audio/Q1/FBn4.mp3",

```




#Anikas Submit Button (The CSS version, of course)" (top notch CSS right hurr)

Add the class disableBtn to your button and just do .removeClass() when you want it to be clickable. put it at the top of the CSS file if not working. 
```
.disabledBtn {
	pointer-events: none;
	cursor: not-allowed;
}
```