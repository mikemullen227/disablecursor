//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++//-----------------------\\+++++++++++++++
//+++++++++++++++||                       ||+++++++++++++++
//+++++++++++++++||   DISCLAIMER:         ||+++++++++++++++
//+++++++++++++++||    Never edit this    ||+++++++++++++++
//+++++++++++++++||     file, thx  :)     ||+++++++++++++++
//+++++++++++++++||     v1.5.20180906     ||+++++++++++++++
//+++++++++++++++\\-----------------------//+++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//Mouseclicksound
var audPlayerMouseClick = new Audio("media/audio/sounds/MouseClick.mp3");
audPlayerMouseClick.volume = 0.8;
var chap_nums = 0;
var audPlayerOne = new Audio("");
var audPlayerTwo = new Audio("");
var audPlayerThree = new Audio("");
var audIndex;
var audSrc = [];
var audUser = false;

//Order of calling the functions to generate the current slide
function functionCalls(){
	$("body").off();
	globalVar.$curSlide = $("section.present");
	globalVar.curSlide = $(".present")[0].id;

	if(slides[globalVar.curSlide].type == "dnd"){
		if(globalVar.dragElements.length != 0){
			globalVar.dragElements.each(function(){
				$(this).draggable( "disable" );
			});
		}
		if(globalVar.dropElements.length != 0){
			globalVar.dropElements.each(function(){				
				$(this).droppable( "disable" );
			});
		}
	}

	randomizer(slides[globalVar.curSlide].type);

	showNavElements();

	dndFunctions();

	slideAudio();

	if($("#Screen_Chart").length != 0){
		chartProgress();
	}
	
	onEnterAction(globalVar.curSlide);
	videoCheck();
	elementsFadingOnSlide();
	developmentStuff();
	actionsOnEverySlideEnter();



	$(".FB_cur").off();
	$(".FB_Pos").off();
	$(".FB_Neg").off();
	$(".FB_Nneg").off();

	//check revealjs.addeventlistener bottom page for dnd resets
	
	$(".icon_size").off();

	$(".icon_size").css("opacity","1");
	$(".icon_size").css("display","block");

	//IE9 Bugfix
	setTimeout(function(){
		$("section.future").css("display","none");
		$("section.past").css("display","none");
	},55);
}


function smar_sendDataToLMS(){
	var str = "";
	//gets all chapter variables
	for(var i = 1;i<=chap_nums;i++){
		if(isNaN(window["C_Q"+i])){
			window["C_Q"+i] = 0;
		}

		if(i < chap_nums){
			str += window["C_Q"+i]+",";
		}else{
			str += window["C_Q"+i];
		}
	}
	
	if(sendDataToLMS().toString().indexOf(",")>0){
		var tempAry = sendDataToLMS().toString().split(",");
	}else{
		var tempAry = [];
		tempAry[0] = sendDataToLMS().toString();
	}
	var _str = "";
	for(var i = 0;i<tempAry.length;i++){
		// console.log(i,parseInt(tempAry[i]));
		if(isNaN(parseInt(tempAry[i]))){
			tempAry[i] = 0;
		}
		if(i<tempAry.length){
			_str += tempAry[i]+",";		
		}
	}
	// console.log(_str+str);
	scorm.set("cmi.suspend_data",_str+str);

	if(globalVar.scormSettings.bookmarking.useBookmarking && globalVar.scormSettings.useScorm){
		if(bookMarkingToLMS()!=""){
			scorm.set("cmi.core.lesson_location",bookMarkingToLMS());
		}else{
			scorm.set("cmi.core.lesson_location","0");

		}
	}

}

function chartProgress() {
	//Code for Screen_Chart (bottom right)
	for(var o = 1;o<=chap_nums;o++){
		// console.log(o,window["C_Q"+o],window["C_Q"+o+"_done"]);
		if(window["C_Q"+o] == 1 && window["C_Q"+o+"_done"] == false){
			window["C_Q"+o+"_done"] = true;
			getPgr();
			}
	}
}

function getPgr() {
	var x = parseFloat($("#Screen_Chart").attr("data-percent")) + 1/chap_nums;
	charts["Screen_Chart"].animate(x);
	$("#Screen_Chart").attr("data-percent", x);
}

function jumpToShuffledSlide(part){
	var n = globalVar.shuffledSlides[part]["pointer"];
	if(n < globalVar.shuffledSlides[part]["slides"].length){
		if(n == null){//first time call
			n = 0;
		}
		jumpToSlide(globalVar.shuffledSlides[part]["slides"][n]);
		n++;
		globalVar.shuffledSlides[part]["pointer"] = n;
	}else{
		console.log("reset your shuffled slides");
	}
}

function resetShuffledSlide(part){
	globalVar.shuffledSlides[part]["pointer"] = null;
}

function randomizeQuestionSlides(){
	if(globalVar.randomizeSlides.useRandomize == true){
		var x = globalVar.randomizeSlides.slides;
		var n = [];
		if(Object.keys(x) != 0){
			for(key in x){
				n = [];
				if(x[key][0] < x[key][1] ){//1st value must be less than 2nd
					for(var i=x[key][0];i<=x[key][1];i++){
						n.push(i);
					}
					shuffle(n);
					n.push(x[key][2]);
					// console.log(n);
					globalVar.shuffledSlides[key] = {slides:n,pointer:null};
				}
			}
		}else{
			console.log("Missing keys and range of randomized slides");
		}
	}
}


function elementsFadingOnSlide(){
	if($('#'+globalVar.curSlide+' [data-fadeInOrder]').length != 0){
		var ary = [];
		$('#'+globalVar.curSlide+' [data-fadeInOrder]').each(function(){
			$(this).css("opacity","0");
			$(this).css("display","none");
			$(this).hide();
			var temp =[];
			var str = $(this).data("fadeinorder");
			temp[0] = str;
			temp[1] = $(this)[0];
			ary.push(temp);
		});
		ary.sort(sortArrayMatrixFunction);
		// console.table(ary);

		for(var i=0;i<ary.length;i++){
			elementsFadeIn(ary[i][1],ary[i][0])
		}
	}

	if($('#'+globalVar.curSlide+' [data-fadeoutorder]').length != 0){
		var ary = [];
		$('#'+globalVar.curSlide+' [data-fadeoutorder]').each(function(){
			$(this).css("opacity","1");
			$(this).css("display","block");
			$(this).show();
			var temp =[];
			var str = $(this).data("fadeoutorder");
			temp[0] = str;
			temp[1] = $(this)[0];
			ary.push(temp);
		});
		ary.sort(sortArrayMatrixFunction);
		// console.table(ary);

		for(var i=0;i<ary.length;i++){
			elementsFadeOut(ary[i][1],ary[i][0])
		}
	}
}

function resetDnD(revertTime,dndAnswers){
	globalVar.$curSlide.find(".draggable").each(function(){
		$(this).animate({
			top:$(this).data('originalTop'),
			left:$(this).data('originalLeft')
		},revertTime);
		$(this).data("dropped",false);
	});
	globalVar.$curSlide.find(".droppable").each(function(){
		$(this).data("hasDrags",false);
		$(this).data("drops",0);
		$(this).data("dragElements",[]);
		$(this).data("dropCounter",slides[globalVar.curSlide]["dnd"]["dropCounter"][this.id]);
	});
	//reset dndAnswersect
	for(key in dndAnswers){
		dndAnswers[key] = [];
	}

	if(slides[globalVar.curSlide].dnd.type == "dnd_2"){
		globalVar.$curSlide.find(".draggable").fadeIn();
		globalVar.$curSlide.find(".draggable").removeClass("shrink");
	}


	// console.log(dndAnswers);
	globalVar.dndAnswers = dndAnswers;
	return dndAnswers;
}

function dndFunctions(){
	if(slides[globalVar.curSlide].type == "dnd"){
		var curDrag;
		var curDrop;
		var outside = false;
		var origPos = {x:0,y:0};
		var rejected = false;
		var i = 0;
		var str ="";
		var $dragElements = globalVar.$curSlide.find(".draggable");
		var $dropElements = globalVar.$curSlide.find(".droppable");

		// console.log($dragElements);
		// console.log($dropElements);
		
		globalVar.dragElements = $dragElements;
		globalVar.dropElements = $dropElements;

		globalVar.dndAnswers = {};

		for(key in slides[globalVar.curSlide]["dnd"]["quiz"]){
			i++;
			// console.log(key,i);
			var stra = '"'+key +'":'+'[]'; 
			if(Object.keys(slides[globalVar.curSlide]["dnd"]["quiz"]).length >= 2){
				// console.log(Object.keys(slides[globalVar.curSlide]["dnd"]["quiz"]).length);
				
				if(i != Object.keys(slides[globalVar.curSlide]["dnd"]["quiz"]).length){
					// console.log(stra);
					stra = stra + ",";
				}
			}
			str = str +  stra;
		}
		str ='{'+ str +'}';
		var dndAnswers = JSON.parse(str);
		//write a blank object/answers into the globalVar
		globalVar.dndAnswers = dndAnswers;
		// console.log(dndAnswers);

		$dragElements.each(function(){				
			$(this).data({
				'originalLeft': Math.round(parseInt($(this).css("left"))/$(this).parent().width()*100).toString()+"%",
				'originalTop': Math.round(parseInt($(this).css("top"))/$(this).parent().height()*100).toString()+"%",
				'dropped':false
			});
			// console.log($(this).data());
			
		});

		$dropElements.each(function(){
			$(this).data({
				'hasDrags' : false,
				'drops': 0,
				'dragElements': [],
				'dropCounter': slides[globalVar.curSlide]["dnd"]["dropCounter"][this.id]
			});
			$(this).data({
				'hasDrags' : false
			});	
		
		});


		$(".si_Reset").on("click touch",function() {
			dndAnswers = resetDnD(revertTime,dndAnswers);
		});
		
		if(slides[globalVar.curSlide]["dnd"].type == "dnd_1"){
			var revertTime = slides[globalVar.curSlide]["dnd"]["revertTime"];
			var dropCounter = slides[globalVar.curSlide]["dnd"]["dropCounter"];
			var snapping = slides[globalVar.curSlide]["dnd"]["snapping"];
		}else if(slides[globalVar.curSlide]["dnd"].type == "dnd_2"){
			var revertTime = 300;
		}

		$dragElements.draggable({
			start: function(event,ui){
				// startFix(event,ui);			
				outside = true;						
				curDrag = ui.helper[0];
				rejected = false;
				if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDragStart"] != undefined){
					slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id].onDragStart();
				}
			},
			drag:function(event,ui){
				// dragFix(event,ui);
				$(curDrag).css("zIndex","4");
				globalVar.$curSlide.find(".dragicondown").fadeTo("fast","0");
				globalVar.$curSlide.find(".dragiconup").fadeTo("fast","0");
				globalVar.$curSlide.find(".dragiconleft").fadeTo("fast","0");
				globalVar.$curSlide.find(".dragiconright").fadeTo("fast","0");
				if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDrag"] != undefined){
					slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id].onDrag();
				}
			},
			revert: function(){
				if(outside == true){
					//revert
					// console.log("outside");
					//if its not already dropped somewhere, then return to orig-pos
					if($(curDrag).data("dropped") == false){
						$(curDrag).animate({
							top:$(this).data('originalTop'),
							left:$(this).data('originalLeft')
						},revertTime);
					}else{
						// console.log("OUTSIDE REVERT");
						
						$(curDrag).animate({
							top:$(this).data('droppedTop'),
							left:$(this).data('droppedLeft')
						},revertTime);
					}
				}else{
					// console.log("inside");
					//not revert
				}
			},
			// containment: "window",
			revertDuration: revertTime,
			stop: function(event,ui){
				$(".draggable").css("zIndex","3");//set all draggable to 3
				$(curDrag).css("zIndex","4");//set the current one to 4
				if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDragEnd"] != undefined){
					slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id].onDragEnd();
				}
				if(curDrop != undefined){//debugging
					// console.log(curDrag.id,curDrop.id)
				}
			}
		});

		$dropElements.droppable({
			accept: ".draggable",
			out: function(event,ui){
				outside = true;				
			},
			over: function(event,ui){
				outside = false;
				curDrop = event.target;
				// console.log($(curDrop).data());
				// console.log(curDrag.id + "    -   "+ curDrop.id);					

				if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDragOver"] != undefined){
					if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDragOver"][curDrop.id] != undefined){
						slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDragOver"][curDrop.id]();
					}							
				}			
			},
			drop: function(event,ui){				
				outside = false;
				//save position from curDrop
				var left = Math.round(parseInt($(curDrop).css("left"))/$(curDrop).parent().width()*100).toString()+"%";
				var top = Math.round(parseInt($(curDrop).css("top"))/$(curDrop).parent().height()*100).toString()+"%";

				// console.log($(curDrag).data("dropped"));
				
				if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDrop"] != undefined){
					if(slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDrop"][curDrop.id] != undefined){
						slides[globalVar.curSlide]["dnd"]["actions"][curDrag.id]["onDrop"][curDrop.id]();
					}							
				}
				if(slides[globalVar.curSlide]["dnd"].type == "dnd_2"){

					// console.log($(curDrop).data("drops"),$(curDrop).data("dropCounter"));

					if($(curDrop).data("drops") < $(curDrop).data("dropCounter")){//as long as you still can drop things
						$(curDrop).data("dragElements").push(curDrag.id); //adds the curDrag to the array
						$(curDrop).data("drops",$(curDrop).data("drops")+1); //adds one to the counter
						$(curDrag).addClass('shrink');
						$(curDrag).data("dropped",true);
					}else{//revert if max is reached
						$(curDrag).animate({
							top:$(curDrag).data('originalTop'),
							left:$(curDrag).data('originalLeft')
						},revertTime);
					}
				}else if(slides[globalVar.curSlide]["dnd"].type == "dnd_1"){
						if($(curDrop).data("drops") < $(curDrop).data("dropCounter")){//if more drags are accepted from the drop target
							// console.log("dropCounter allows more to be dropped");
							//snapping alignment goes here
							if($(curDrag).data("dropped") == false){//if it is not dropped yet, then add this right away
								// console.log("curDrag not dropped");
								
								$(curDrag).data("dropped",true);
								$(curDrop).data("dragElements").push(curDrag.id); //adds the one to the array
								$(curDrop).data()["drops"] += 1; //adds one to the counter
								//snapping the new one
								if(snapping == true){
									$(curDrag).css('top', top);
									$(curDrag).css('left', left);
								}									
								//and store it into the data
								$(curDrag).data({
									'droppedLeft': Math.round(parseInt($(curDrag).css("left"))/$(curDrag).parent().width()*100).toString()+"%",
									'droppedTop': Math.round(parseInt($(curDrag).css("top"))/$(curDrag).parent().height()*100).toString()+"%",
								});
							}else{//if it was already dropped, then delete curDrag from his drag target and apply it to curDrop
								// console.log("curDrag already dropped");
								
								var curChangeEl =  $("#"+$(curDrop).data("dragElements")[$(curDrop).data("dragElements").length-1])[0];
								//get Drop where curDrag is stored
								var tempDrop;
								globalVar.$curSlide.find(".droppable").each(function(){
									// console.log(this.id,$(this).data("dragElements"));
									if($(this).data("dragElements").indexOf(curDrag.id) >= 0){
										// console.log("THIS DROP HAS THE SEARCHED CURDRAG",this.id)
										tempDrop =this; 
									}
								});
								if(snapping == true){
									$(curDrag).css('top', top);
									$(curDrag).css('left', left);
								}					
								//and store it into the data
								$(curDrag).data({
									'droppedLeft': $(curDrag).css('left'),
									'droppedTop': $(curDrag).css('top')
								});

								//remove curDrag from tempDrop
								var index = $(tempDrop).data("dragElements").indexOf(curDrag.id);							
								$(tempDrop).data("dragElements").splice(index, 1);
								$(tempDrop).data()["drops"] -= 1; //remove one from the counter
								
								//adds the curDrag to the array
								$(curDrop).data("dragElements").push(curDrag.id);
								$(curDrop).data()["drops"] += 1; //adds one to the counter

							}
						}else{
							// console.log("sorry, dropelement ist voll");							
							if($(curDrag).data("dropped") == false){//if curDrag isn't dropped anywhere else, revert curChangeEl always to originalPos

								if(!($(curDrop).data("dragElements").indexOf(curDrag.id) >=0)){//if the element isn't already dropped there
									var curChangeEl =  "#"+$(curDrop).data("dragElements")[$(curDrop).data("dragElements").length-1];		
									//animate the to changed element back to original (always the last dropped one)
									$(curChangeEl).animate({
										top:$(curChangeEl).data('originalTop'),
										left:$(curChangeEl).data('originalLeft')
									},revertTime);

									//snapping the new one
									if(snapping == true){
										$(curDrag).css('top', top);
										$(curDrag).css('left', left);
									}							
									//and store it into the data
									$(curDrag).data({
										'droppedLeft': Math.round(parseInt($(curDrag).css("left"))/$(curDrag).parent().width()*100).toString()+"%",
										'droppedTop': Math.round(parseInt($(curDrag).css("top"))/$(curDrag).parent().height()*100).toString()+"%",
									});
									//curDrag dropped
									$(curDrag).data("dropped",true)
									//changed element not dropped 
									$(curChangeEl).data("dropped",false)
									
									var index = $(curDrop).data("dragElements").indexOf(curChangeEl.id);							
									$(curDrop).data("dragElements").splice(index, 1);//remove the curChangeEl

									$(curDrop).data("dragElements").push(curDrag.id); //adds the curDrag to the array
								}else{//element already dropped there ( for bugfixes)
									if(snapping == true){
										$(curDrag).css('top', top);
										$(curDrag).css('left', left);
										
										//and store it into the data
										$(curDrag).data({
											'droppedLeft': $(curDrag).css('left'),
											'droppedTop': $(curDrag).css('top')
										});
									}//end snapping
								}//end else
							}else{ //if curDrag is also dropped, swap positions from their droppedPos
								var curChangeEl =  $("#"+$(curDrop).data("dragElements")[$(curDrop).data("dragElements").length-1])[0];
								var tempDrop; //tempDrop is the drop from the drag
								// console.log("both dropped");

								globalVar.$curSlide.find(".droppable").each(function(){
									// console.log(this.id,$(this).data("dragElements"));
									if($(this).data("dragElements").indexOf(curDrag.id) >= 0){
										// console.log("THIS DROP HAS THE SEARCHED CURDRAG",this.id)
										tempDrop =this; 
									}
								});			
								
								var tempLeft = Math.round(parseInt($(tempDrop).css("left"))/$(tempDrop).parent().width()*100).toString()+"%";
								var tempTop = Math.round(parseInt($(tempDrop).css("top"))/$(tempDrop).parent().height()*100).toString()+"%";
				
								$(curChangeEl).animate({
									top:tempTop,
									left:tempLeft
								},revertTime);

								//snapping the new one
								if(snapping == true){
									$(curDrag).css('top', top);
									$(curDrag).css('left', left);
									
									//and store it into the data
									$(curDrag).data({
										'droppedLeft': Math.round(parseInt($(curDrag).css("left"))/$(curDrag).parent().width()*100).toString()+"%",
										'droppedTop': Math.round(parseInt($(curDrag).css("top"))/$(curDrag).parent().height()*100).toString()+"%",
									});
								}								
								
								//remove curDrag from tempDrop
								var index = $(tempDrop).data("dragElements").indexOf(curDrag.id);							
								$(tempDrop).data("dragElements").splice(index, 1);
								//and add curChangeEl to tempDrop
								$(tempDrop).data("dragElements").push(curChangeEl.id); //adds the curDrag to the array

								//remove curChangeEl from curDrop
								var index = $(curDrop).data("dragElements").indexOf(curChangeEl.id);							
								$(curDrop).data("dragElements").splice(index, 1);
								//and add curDrag to curDrop
								$(curDrop).data("dragElements").push(curDrag.id); //adds the curDrag to the array

								// console.log($(curDrag));
								

								if($(curDrag).data("dropped") == false){
									// console.log("teststet");
									
									$(tempDrop).data()["drops"] -= 1; //remove one from the counter
									$(curDrop).data()["drops"] += 1; //adds one to the counter
								}else{
									// console.log("qweqweqwe");
									
								}

						}
					}
				//debugging
				// globalVar.$curSlide.find(".droppable").each(function(){
				// 	console.log(this.id,$(this).data("dragElements"));
				// 	if($(this).data("dragElements").indexOf(curDrag.id) >= 0){
				// 		console.log("THIS DROP HAS THE SEARCHED CURDRAG",this.id)
				// 		tempDrop =this; 
				// 	}
				// });
				// globalVar.$curSlide.find(".droppable").each(function(){
				// 	console.log(this.id,$(this).data("drops"));
				// });
			}//end else dnd1/dnd2


				for(key in slides[globalVar.curSlide]["dnd"]["quiz"]){					
					dndAnswers[key] = $("#"+key).data("dragElements");
				}
				// console.log(dndAnswers);
				
			}//end drop
		});

		globalVar.dragElements.each(function(){
			$(this).draggable( "enable" );
		});
	
		globalVar.dropElements.each(function(){
			$(this).droppable( "enable" );
		});
	}
}


function QCheck(type) {
	var bol = true;
  if(type =="scq"){
		for(key in slides[globalVar.curSlide][type]["quiz"]){
			bol2=true;
			if ($("#"+globalVar.curSlide).find(".sc-answer-clicked")[0] != undefined) {
				var answer = $("#"+globalVar.curSlide).find(".sc-answer-clicked").children().eq(1).html();
				if (slides[globalVar.curSlide][type]["quiz"][key][answer] == "1") {
					bol2 = true;
					bol = true;
				} else {
					bol2 = false;
					bol = false;
				}
			} else {
				bol2 = false;
				bol=false;
			}

			if(bol == true){
				break;
			}		
		}
		disableControls();
		return bol;
	}else{
		if(type=="mcq"){
			for(key in slides[globalVar.curSlide][type]["quiz"]){
				bol2 = true;
				$("#"+globalVar.curSlide).find(".mc-answer").each(function () {
						// console.log($(this).children().eq(1).html(),slides[globalVar.curSlide][type]["quiz"][key][$(this).children().eq(1).html()]);
					if(bol2==true){
						if ($(this).hasClass("mc-answer-clicked")) {
							if (slides[globalVar.curSlide][type]["quiz"][key][$(this).children().eq(1).html()] != 1) {
								// console.log("geklickt aber nicht richtig");
								bol2 = false;
								bol = false;
							} else {
								// console.log("geklickt und richtig");
								bol=true;
							}
						} else {
							// console.log("ungeklickt");
							if (slides[globalVar.curSlide][type]["quiz"][key][$(this).children().eq(1).html()] != 0) {
								bol2 = false;
								bol = false;
							}
						}//ifclicked
						// console.log(bol2, bol);						
					}
				});
				// console.log("    ",bol);
				if(bol == true){
					break;
				}
			}

			disableControls();
			return bol;
		}else{
			if(type=="dnd"){
				bol2 = false;
				var userAnswers = globalVar.dndAnswers;
				var test = slides[globalVar.curSlide][type]["quiz"];
				// console.log(userAnswers);
				// console.log(test);
							
			 //assume that the user answered it correctly (bol = true)
				for(key in slides[globalVar.curSlide][type]["quiz"]){
					//loop through all 
					if(Object.keys(slides[globalVar.curSlide][type]["quiz"]).length == 1 && Object.keys(slides[globalVar.curSlide][type]["quiz"][key]).length ==1){
					
						//if there is only one drop target and one answer array
						for(key2 in slides[globalVar.curSlide][type]["quiz"][key]){
							// console.log("key#",key2);
							// console.log(slides[globalVar.curSlide][type]["quiz"][key]);
							// console.log(slides[globalVar.curSlide][type]["quiz"][key][key2]);
							
							for(var i = 0;i<slides[globalVar.curSlide][type]["quiz"][key][key2].length;i++){
								// console.log("  ",userAnswers[key][i]);
								// console.log(slides[globalVar.curSlide]["dnd"]["quiz"][key][key2]);
								// console.log(slides[globalVar.curSlide]["dnd"]["quiz"][key][key2].indexOf(userAnswers[key][i]));
								if(bol == true){
									if(slides[globalVar.curSlide][type]["quiz"][key][key2].indexOf(userAnswers[key][i]) != -1){
										bol = true;
										// console.log("juhu");
									}else{
										// console.log("nope");
										bol = false;
									}
								}else{
									// console.log("da war mal was falsch");
									bol = false;
								}
							}
						}

					}else{
						//if there are multiple arrays with possible answers
						if(bol == true){
							bol2 = false;
							for(key2 in slides[globalVar.curSlide]["dnd"]["quiz"][key]){

								for(var i = 0;i<slides[globalVar.curSlide][type]["quiz"][key][key2].length;i++){
									// console.log("User ",userAnswers[key][i],"    WE",slides[globalVar.curSlide][type]["quiz"][key][key2]);			
									if(bol2 == false){
										if(slides[globalVar.curSlide][type]["quiz"][key][key2].indexOf(userAnswers[key][i]) != -1){
											bol2 = true;
											// console.log("juhu");
										}else{
											// console.log("nope");
											bol=false;
										}
									}else{
										// console.log("da war mal was richtig");
										bol2 = true;
										bol = true
									}
								}
							}
						}else{
							bol = false;
						}

						if(bol2==true){
							bol = true;
						}
							// console.log("<  --  >");
							// console.log("bol2  ",bol2);
							// console.log("bol  ",bol);						
						}

					}
				
					// console.log(bol);
					disableControls();
					return bol;

			}else{
				//every other slide type goes here
			}
		}//else
	}//else
}//Qcheck

//Pos FB check, executes whatever is inside onSuccessAction
function FBPos(){	
	$("#"+globalVar.curSlide).find(".FB_Pos").fadeIn();
	$("#"+globalVar.curSlide).find(".FB_Pos").addClass("FB_cur");
	
	var type = slides[globalVar.curSlide].type;
	slides[globalVar.curSlide][type]["trys"] = 0;

	$(audPlayerOne).trigger("pause");
	if(slides[globalVar.curSlide]["audio"]["FBPos"] != undefined){
		audPlayerOne.src = slides[globalVar.curSlide]["audio"]["FBPos"];
		if (audUser == false) {
			audPlayerOne.play();
		}
	}else{
		console.log("no audio")
	}


	setTimeout(function () {
		audPlayerThree.src = globalVar.correctSound;
		audPlayerThree.volume = 0.5;

		if (audUser == false) {
			audPlayerThree.play();
		}
	}, 50);
}

//Neg and Nneg FB check, executes whatever is inside onFailureAction1 or onFailureAction2
function FBNeg(){
	// console.log(" :( negatives Feedback");
	var type = slides[globalVar.curSlide].type;
	var x = slides[globalVar.curSlide][type]["trys"];
	// console.log(x);
	x++;
	slides[globalVar.curSlide][type]["trys"] = x;
	// console.log(slides[globalVar.curSlide][type]["trys"]);
	// console.log("attempts", slides[globalVar.curSlide][type]["userAttempts"]);
	var attempts = slides[globalVar.curSlide][type]["userAttempts"];
	
	if(attempts == 1){
		if (slides[globalVar.curSlide][type]["trys"] == 1) {
			$("#"+globalVar.curSlide).find(".FB_Neg").fadeIn();
			$("#"+globalVar.curSlide).find(".FB_Neg").addClass("FB_cur");
			slides[globalVar.curSlide][type]["trys"] = 0;

			$(audPlayerOne).trigger("pause");
			if(slides[globalVar.curSlide]["audio"]["FBNeg"] != undefined){
				audPlayerOne.src = slides[globalVar.curSlide]["audio"]["FBNeg"];
			}else{
				console.log("no audio");
			}

			if (audUser == false) {
				audPlayerOne.play();
			}
			if(slides[globalVar.curSlide]["onFailureAction1"] != undefined){
				if(slides[globalVar.curSlide]["onFailureAction1"] != ""){
					slides[globalVar.curSlide].onFailureAction1();
				}
			}

			setTimeout(function () {
				var audPlayerFour = new Audio("");
				audPlayerFour.src = globalVar.wrongSound;
				audPlayerFour.volume = 0.5;

				if (audUser == false) {
					audPlayerFour.play();
				}
			}, 200);
		}
	}

	if(attempts == 2){
		if (slides[globalVar.curSlide][type]["trys"] == 1) {
			$("#"+globalVar.curSlide).find(".FB_Neg").fadeIn();
			$("#"+globalVar.curSlide).find(".FB_Neg").addClass("FB_cur");

			$(audPlayerOne).trigger("pause");
			if(slides[globalVar.curSlide]["audio"]["FBNeg"] != undefined){
				audPlayerOne.src = slides[globalVar.curSlide]["audio"]["FBNeg"];
				if (audUser == false) {
					audPlayerOne.play();
				}
			}else{
				console.log("no audio");
			}

			if(slides[globalVar.curSlide]["onFailureAction1"] != undefined){
				if(slides[globalVar.curSlide]["onFailureAction1"] != ""){
					slides[globalVar.curSlide].onFailureAction1();
				}
			}

			setTimeout(function () {
				var audPlayerFour = new Audio("");
				audPlayerFour.src = globalVar.wrongSound;
				audPlayerFour.volume = 0.5;

				if (audUser == false) {
					audPlayerFour.play();
				}
			}, 200);
		}
		if (slides[globalVar.curSlide][type]["trys"] == 2) {
			$("#"+globalVar.curSlide).find(".FB_Nneg").fadeIn();
			$("#"+globalVar.curSlide).find(".FB_Nneg").addClass("FB_cur");
			slides[globalVar.curSlide][type]["trys"] = 0;
			$(audPlayerOne).trigger("pause");

			if(slides[globalVar.curSlide]["audio"]["FBNneg"] != undefined){
				audPlayerOne.src = slides[globalVar.curSlide]["audio"]["FBNneg"];
			}else{
				console.log("no audio");
			}

			if (audUser == false) {
				audPlayerOne.play();
			}

			if(slides[globalVar.curSlide]["onFailureAction2"] != undefined){
				if(slides[globalVar.curSlide]["onFailureAction2"] != ""){
					slides[globalVar.curSlide].onFailureAction2();
				}
			}

			setTimeout(function () {
				var audPlayerFour = new Audio("");
				audPlayerFour.src = globalVar.wrongSound;
				audPlayerFour.volume = 0.5;

				if (audUser == false) {
					audPlayerFour.play();
				}
			}, 200);


		}
	}
}

//calls the FBPos or FBn function, executes the onSuccessAction of the object and sets the onclick function for the current FB
function FBCheck(bol) {
	disableControls();
	if (bol == true) {
		FBPos();
		slides[globalVar.curSlide].onSuccessAction();
	} else {
		FBNeg();
	}

	$(".FB_cur").on("click touch", function () {
		//Reset MCQ/SCQ clicked answers
		resetMCQSCQ();
		
		audPlayerMouseClick.play();
		$(this).fadeOut().hide();
		enableControls();
		$(audPlayerOne).trigger("pause");
		// console.log("asiduh");
		
		if(slides[globalVar.curSlide].type == "dnd"){
			resetDnD(slides[globalVar.curSlide].revertTime,globalVar.dndAnswers)
		}
		// console.log($(this));
		// console.log($(this).attr("class"));
		
		var classList = $(this).attr("class").split(/\s+/);
		$.each(classList, function (index, item) {
			
			FBClick(item);
			
		});

		$(this).removeClass("FB_cur");


	});
}

function resetMCQSCQ(){
	$(".mc-answer-clicked").each(function(){
		var icon = $(this).find(".mcq-button");
		if ($(this).hasClass("mc-answer-clicked")) { //If clicked
			$(this).removeClass("mc-answer-clicked");
			icon.removeClass("mcq-button-clicked").addClass("mcq-button-down");
			icon.removeClass("mcq-button-over");
			icon.removeClass("mcq-button-down").addClass("mcq-button-up");
		}
	});
	$(".sc-answer-clicked").each(function(){
		var icon = $(this).find(".scq-button");
		$(".sc-answer-clicked").removeClass("sc-answer-clicked");
		$(this).addClass("sc-answer-clicked");

		icon.removeClass("scq-button-up").addClass("scq-button-down");
		icon.removeClass("scq-button-down").addClass("scq-button-clicked");
		$(this).parent().find("li").find(".scq-button-clicked").removeClass("scq-button-clicked scq-button-over").addClass("scq-button-up");
	});
}

//Show or hide navigation elements (inside data.js)
function showNavElements() {
	$("#smar_Next").hide();
	$("#smar_Back").hide();
	$("#smar_Replay").hide();
	$("#smar_Info").hide();
	$("#smar_Home").hide();
	$("#smar_Exit").hide();
	$("#smar_Menu").hide();
	$("#smar_Play").hide();

	if($("#Screen_Chart").length != 0){
		$("#Screen_Chart").hide();
	}

	var t = slides[globalVar.curSlide].navElements
	var l = t.length;
	// console.log(t);
	
	if(l > 0){
		for(var i = 0;i<l;i++){
			switch(t[i]){
				case "Chart": 
					t[i] = "Screen_Chart";					
					$("#" + t[i]).show();
					break;
				case "Screen_Chart": 
					t[i] = "Screen_Chart";
					// console.log("aoisdhj");
					
					$("#" + t[i]).show();
					break;
				case "Play":
					if (audUser == false) {
						$("#smar_sub_Play").hide();
						$("#smar_sub_Pause").show();
					}else{
						$("#smar_Play").show();
						$("#smar_sub_Pause").hide();
					}
					$("#smar_" + t[i]).show();
					break;
				default :
					$("#smar_" + t[i]).show();
					break;
			}
		}
	}
}

//Hover and Click for MCQ and SCQ
function mcqHover() {
	$(".mc-answer").hover(function () {
		var icon = $(this).find(".mcq-button");
		// console.log(icon);
		if ($(this).hasClass("mc-answer-clicked")) {
			icon.removeClass("mcq-button-clicked").addClass("mcq-button-up");
		} else {
			icon.removeClass("mcq-button-up").addClass("mcq-button-over");

		}
	}, function () {
		var icon = $(this).find(".mcq-button");
		if ($(this).hasClass("mc-answer-clicked")) {
			icon.removeClass("mcq-button-up").addClass("mcq-button-clicked");
		} else {
			icon.removeClass("mcq-button-over").addClass("mcq-button-up");
		}
	});
}

function mcqClick() {
	$(".mc-answer").on("click touch", function () {
		audPlayerMouseClick.play();
		var icon = $(this).find(".mcq-button");
		if ($(this).hasClass("mc-answer-clicked")) { //If clicked
			$(this).removeClass("mc-answer-clicked");
			icon.removeClass("mcq-button-clicked").addClass("mcq-button-down");
			icon.removeClass("mcq-button-over");
			setTimeout(function () {
				icon.removeClass("mcq-button-down").addClass("mcq-button-up");
			}, 55);
		} else { //if its not clicked 
			$(this).addClass("mc-answer-clicked");
			icon.removeClass("mcq-button-up").addClass("mcq-button-down");
			setTimeout(function () {
				icon.removeClass("mcq-button-down").addClass("mcq-button-clicked");
			}, 55);
		}
	});
}

function scqHover() {
	$(".sc-answer").hover(function () {
		var icon = $(this).find(".scq-button");
		if ($(this).hasClass("sc-answer-clicked")) {
			icon.removeClass("scq-button-clicked").addClass("scq-button-up");
		} else {
			icon.removeClass("scq-button-up").addClass("scq-button-over");

		}
	}, function () {
		var icon = $(this).find(".scq-button");
		if ($(this).hasClass("sc-answer-clicked")) {
			icon.removeClass("scq-button-up").addClass("scq-button-clicked");
		} else {
			icon.removeClass("scq-button-over").addClass("scq-button-up");
		}
	});
}

function scqClick() {
	$(".sc-answer").on("click touch", function () {
		audPlayerMouseClick.play();
		var icon = $(this).find(".scq-button");
		$(".sc-answer-clicked").removeClass("sc-answer-clicked");
		$(this).addClass("sc-answer-clicked");

		icon.removeClass("scq-button-up").addClass("scq-button-down");
		setTimeout(function () {
			icon.removeClass("scq-button-down").addClass("scq-button-clicked");
		}, 55);
		$(this).parent().find("li").find(".scq-button-clicked").removeClass("scq-button-clicked scq-button-over").addClass("scq-button-up");
	});
}

//MCQ/SCQ Randomizer
function randomizer(type){
	rngAry = [];
	// console.log(globalVar.curSlide);
	var ul_class,li_class_1,li_class_2;
	if(type == "mcq" || type == "scq"){
		switch (type){
			case "mcq": ul_class = ".mcq"; li_class_1="mc-answer";li_class_2="mcq-button mcq-button-up";
					break;
			case "scq":ul_class = ".scq"; li_class_1="sc-answer";li_class_2="scq-button scq-button-up";
					break;
			case "dnd":
					break;		
			default: break;
		}
	}else{
		return false;
	}

	$("#" + globalVar.curSlide).find(ul_class).empty();

		if (slides[globalVar.curSlide][type]["randomize"] == true) {
			for (key in slides[globalVar.curSlide][type]["quiz"]["1"]) {
				// console.log(key);
				rngAry.push(key);
			}

			shuffle(rngAry);
			
			for (var i = 0; i < rngAry.length; i++) {
				// console.log(slides[globalVar.curSlide]["mcq"]["audio"][rngAry[i]]);
				li = document.createElement("li");
				$(li).addClass(li_class_1);
								
				if (globalVar.idbehold_on && slides[globalVar.curSlide][type]["quiz"]["1"][rngAry[i]] == '1') {
					$(li).css('color','red');
				}

				div_o = document.createElement("div");
				$(div_o).addClass(li_class_2);

				$(li).append(div_o);

				div_t = document.createElement("div");
				$(div_t).html(rngAry[i]);

				$(li).append(div_t);

				$("#" + globalVar.curSlide).find(ul_class).eq(0).append(li);
			}
		} else {
			for (key in slides[globalVar.curSlide][type]["quiz"]["1"]) {
				li = document.createElement("li");
				$(li).addClass(li_class_1);
				// console.log(slides[globalVar.curSlide][type]["quiz"]["1"][key]);
				// console.log(key);
				// console.log(li);
				// console.log(globalVar.idbehold_on);
				// console.log(slides[globalVar.curSlide][type]["quiz"]["1"][key] == '1');
				
				if (globalVar.idbehold_on && slides[globalVar.curSlide][type]["quiz"]["1"][key] == '1') {
					$(li).css('color','red');
				}

				div_o = document.createElement("div");
				$(div_o).addClass(li_class_2);

				$(li).append(div_o);
				div_t = document.createElement("div");
				$(div_t).html(key);

				$(li).append(div_t);

				$("#" + globalVar.curSlide).find(ul_class).eq(0).append(li);
			}
		}
		if(type =="scq"){
			scqHover();
			scqClick();
		}
		if(type =="mcq"){
			mcqHover();
			mcqClick();
		}
		// return rngAry;
		globalVar.randomizedArray = rngAry;
}

function videoCheck() {
	var type = slides[globalVar.curSlide].type;
	if(type =="video"){
		var video = globalVar.$curSlide.find("video")[0];
		var videoID = globalVar.$curSlide.find("video")[0].id;
	
		if(slides[globalVar.curSlide].videoLoaded == false){
			videojs(videoID).videoJsResolutionSwitcher();

			videojs(videoID).ready(function(){
				var myPlayer = this;
		
				myPlayer.currentResolution("720p");

				if(this.textTracks().tracks_.length > 0){
					var subtitleSettings = {
						'backgroundOpacity': '0'
						, 'edgeStyle': 'dropshadow'
						, 'color': '#FFF'
						, 'backgroundColor': '#FFF'
						, 'textOpacity': '1'
						, 'windowOpacity': '0'
						, 'fontFamily': 'default'
						, 'windowColor': '#ffffff'
						, 'fontPercent': 1
					};
					videojs(videoID).textTrackSettings.setValues(subtitleSettings);
				}

				slides[globalVar.curSlide].videoLoaded = true;

				myPlayer.play();


				if (slides[globalVar.curSlide].videoSeeking == false) {
					// console.log('Controls Enabled Interactive.jsL23');
					var supposedCurrentTime = 0;
					video.addEventListener('timeupdate', function () {
						if (!video.seeking) {
							supposedCurrentTime = video.currentTime;
						}
					});
					//allows backwards movement of trackbar but can't move trackbar forward
					video.addEventListener('seeking', function () {
						if (video.currentTime > supposedCurrentTime) {
							video.currentTime = supposedCurrentTime;
						}
					});
				}				
		});
		videojs(videoID).on("ended",function(){
			globalVar.videoSeen[globalVar.curSlide] = true;	
			slides[globalVar.curSlide].nextAction();
		});
	}else{		
		videojs(videoID).currentTime(0);
		videojs(videoID).play();
	}

	}else{
		if($("section").find("video").length > 0){
			$("section").find("video")[0].pause();
		}
	}
}

function slideAudio() {
	var type = slides[globalVar.curSlide].type;
	var rngAry = globalVar.randomizedArray;
	setTimeout(function () {
		audSrc = [];
		$(audPlayerOne).trigger("pause");
		// console.log('AUDIO PLYR TRIGGR PAUSE');
		$(audPlayerTwo).trigger("pause");
		// console.log('AUDIO PLYR TRIGGR PAUSE');
		$(audPlayerThree).trigger("pause");
		// console.log('AUDIO PLYR TRIGGR PAUSE');

		audIndex = 0;

		if($.isEmptyObject(slides[globalVar.curSlide].audio) != true){
			if (slides[globalVar.curSlide].audio != undefined) {
				audSrc.push(slides[globalVar.curSlide]["audio"]["onEnter"]); //get onEnter Audio

				if (slides[globalVar.curSlide].type == "mcq" || slides[globalVar.curSlide].type == "scq" ) {
					if (slides[globalVar.curSlide][type].randomize == true) {
						for (i = 0; i < rngAry.length; i++) {
							audSrc.push(slides[globalVar.curSlide][type]["audio"][rngAry[i]]);
						}
					} else {
						for (key in slides[globalVar.curSlide][type].audio) {
							audSrc.push(slides[globalVar.curSlide][type]["audio"][key]);
						}
					}
				}

				if (slides[globalVar.curSlide]["audio"]["onEnd"] != undefined){
					if(slides[globalVar.curSlide]["audio"]["onEnd"] != "") {
						audSrc.push(slides[globalVar.curSlide]["audio"]["onEnd"]); //get onEnd Audio
					}
				}				

				if (audSrc.length == 1) { //Gibt nur ein Audio 
					audPlayerOne = new Audio(audSrc[0]);
					if (audUser != true) {
						audPlayerOne.play();
					}
				} else {
					//start playing audio 0 - normally the only audio
					audPlayerOne = new Audio(audSrc[audIndex]);
					audPlayerOne.volume = 0.8;
					if (audUser != true) {
						audPlayerOne.play();
					}
					audIndex++;
					audPlayerOne.onended = function () { //if audio ends

						if (audSrc[audIndex] != undefined) {
							audPlayerOne.src = audSrc[audIndex];
							if (audUser != true) {
								audPlayerOne.play();
							}
						}
						audIndex++;

						if (audIndex > slides[globalVar.curSlide].audio.length) {
							$(audPlayerOne).trigger("pause");
						}
					};
				}
			}
		}else{
			console.log("no slideaudio");
		}
	}, 150);

}

//it just checks if the framework has all the data it needs, not checking typos
function datajsCheck(){
	var snums = [];
	for(key in slides){
		tempPush = [];
		tempPush[0] = slides[key].slideNumber;
		tempPush[1] = key;
		snums.push(tempPush);
	}



	//Start checking
	for(key in slides){
		console.log(key);

		if($("#"+key).length >0){

			for(var i = 0; i<snums.length;i++){
				if(snums[i][0] == slides[key].slideNumber && key != snums[i][1] ){
					console.log("   The following slide has the same slidenumber: " + snums[i][1]);
				}
			}

			//type check
			if(slides[key].type == undefined){
				console.log("   You are missing an object: type");
			}else{	
				switch(slides[key].type){
					case "mcq": break;
					case "scq": break;
					case "cq": break;
					case "dnd": break;
					case "text": break;
					case "video": break;
					default: console.log("   The type is not correct: "+slides[key].type);break;
				}
			}
			//nav check
			if(slides[key].navElements == undefined){
				console.log("   You are missing an object: navElements");
			}else{			
				for(var i = 0;i<slides[key].navElements.length;i++){
					switch (slides[key].navElements[i]){
						case "Back":break;
						case "Next":break;
						case "Home":break;
						case "Info":break;
						case "Menu":break;
						case "Chart":break;
						case "Screen_Chart":break;
						case "Play":break;
						case "Replay":break;
						case "Exit":break;
						default: console.log("   There is a wrong navigation element: "+slides[key].navElements[i]);
					}
				}
			}

			if(slides[key].backAction == undefined){
				console.log("   You are missing an object: backAction");
			}
			if(slides[key].nextAction == undefined){
				console.log("   You are missing an object: nextAction");
			}
			if(slides[key].onEnterAction == undefined){
				console.log("   You are missing an object: onEnterAction");
			}
			// console.log(slides[key].type,slides[key].scq );

			if(slides[key].type == undefined || slides[key][slides[key].type] ==undefined){
				switch(slides[key].type){
					case "mcq": tempBol = true; break;
					case "scq": tempBol = true; break;
					case "cq": tempBol = true; break;
					case "dnd": tempBol = true; break;
					case "text": tempBol = true; break;
					case "video": tempBol = true; break;
					default: tempBol = false; break;
				}			
				if( tempBol == true){				
					if(slides[key].type != "text" && slides[key].type != "video"){
						if(slides[key][slides[key].type] == undefined){
							console.log("   You are missing an object: "+slides[key].type);
						}
					}
				}
			}else{
				if(slides[key].type == "mcq" || slides[key].type == "scq"){
					
					if(slides[key][slides[key].type].quiz != undefined){
						if(Object.keys(slides[key][slides[key].type].quiz).length !=0){
						for(key2 in slides[key][slides[key].type].quiz){						
							if(Object.keys(slides[key][slides[key].type]["quiz"][key2]).length <= 1){
								console.log("   You are missing a answers, at least 2 ");
							}else{
								if(slides[key].type == "scq"){
									var tempZU = false;
									for(key3 in slides[key][slides[key].type]["quiz"][key2]){
										if(slides[key][slides[key].type]["quiz"][key2][key3]==0 || slides[key][slides[key].type]["quiz"][key2][key3] == 1){
											if(slides[key][slides[key].type]["quiz"][key2][key3] == 1){
												if(tempZU == true){
													console.log("   You have a (at least) more than one correct answer in object: "+key2 +" - "+ key3);
												}else{
													tempZU = true;											
												}
											}
										}else{
											console.log("   You set an incorrect value on a quiz object. You can only assign the number 1(correct) or 0(wrong). You have a: " + slides[key][slides[key].type]["quiz"][key2][key3] );
										}
									}
								}//only for scq
								if(slides[key].type == "mcq"){
									var tempZU = false;
									for(key3 in slides[key][slides[key].type]["quiz"][key2]){
										if(!(slides[key][slides[key].type]["quiz"][key2][key3]==0 || slides[key][slides[key].type]["quiz"][key2][key3] == 1)){
											console.log("   You set an incorrect value on a quiz object. You can only assign the number 1(correct) or 0(wrong). You have a: " + slides[key][slides[key].type]["quiz"][key2][key3] );
										}
									}
								}//only for mcq
							}
							// console.log(Object.keys(slides[key][slides[key].type].quiz[i]));
						}
					}else{
						console.log("   You are missing at least an object: 1 ");

					}
					}else{
						console.log("   You are missing an object: quiz");
					}

					if(slides[key][slides[key].type].randomize == undefined){
						console.log("   You are missing a key: randomize");
					}else{
						if(typeof(slides[key][slides[key].type].randomize) != "boolean"){
							console.log("   Only the boolean true or false is allowed on randomize: "+ slides[key][slides[key].type].randomize);
						}
						// console.log(slides[key][slides[key].type].randomize);
					}
					if(slides[key][slides[key].type].audio == undefined){
						console.log("   You are missing an object: audio (VO for answers)");
					}else{
						if(Object.keys(slides[key][slides[key].type].audio).length == 0){
							console.log("   Warning! There is no answer audio, but this might be intended!");
						}else{						
							if(slides[key][slides[key].type]["quiz"]["1"] != undefined){
								if(Object.keys(slides[key][slides[key].type].audio).length != Object.keys(slides[key][slides[key].type]["quiz"]["1"]).length){
									console.log("   You have voice over, but the number of VO's doesn't fit with your number of answers");
								}
							}
						}
					}

					if(slides[key][slides[key].type].userAttempts == undefined){
						console.log("   You are missing a key: userAttempts");
					}else{
						if(!(slides[key][slides[key].type].userAttempts >= 1 && slides[key][slides[key].type].userAttempts <= 2)){
							console.log("   You have a wrong value in userAttempts. It can only be 1 or 2. You have: " + slides[key][slides[key].type].userAttempts );
						}
					}

					if(slides[key][slides[key].type].trys == undefined){
						console.log("   You are missing a key: trys //dontedit");
					}else{
						if(slides[key][slides[key].type].trys != 0){
							console.log("   You have to set trys to 0. You have: " + slides[key][slides[key].type].trys );
						}
					}

				}//end mcq/scq
				
				if(slides[key].type == "dnd"){
					if(slides[key][slides[key].type].quiz != undefined){
						if(Object.keys(slides[key][slides[key].type].quiz).length >0){
							var tAry=[];
							var tempB = true;
							for(key2 in slides[key][slides[key].type].quiz){
								// console.log(key2,slides[key][slides[key].type]["quiz"][key2]);
								if(!($("#"+key2).length)){
									console.log("   The name of the dropsource wasn't found in the index.html: "+key2);
								}
								// console.log(key2);
								// console.log(slides[key][slides[key].type]["quiz"][key2]);
								if(Object.keys(slides[key][slides[key].type]["quiz"][key2]).length > 0){
									for(key3 in slides[key][slides[key].type]["quiz"][key2]){
										// console.log(key3);
										// console.log(slides[key][slides[key].type]["quiz"][key2][key3]);
										
										if(hasDuplicates(slides[key][slides[key].type]["quiz"][key2][key3])==true){
											console.log("   There are duplicates in one of your answer arrays: "+key2 + " - "+key3);
										}
										for(var i=0;i<slides[key][slides[key].type]["quiz"][key2][key3].length;i++){										
											tAry.push(slides[key][slides[key].type]["quiz"][key2][key3][i]);
										}
										// hasDuplicates(slides[key][slides[key].type]["quiz"][key2][key3]);
									}								
									for(var o=0;o<tAry.length;o++){
										if(!($("#"+tAry[o]).length > 0)){
											console.log("   The name of the dragsource wasn't found in the index.html: "+tAry[o]);
										}
									}
									// console.log(Object.keys(slides[key][slides[key].type]["quiz"][key2]).length);
									if(Object.keys(slides[key][slides[key].type]["quiz"][key2]).length != 1){
										tempB = false;
									}
									
									if(hasDuplicates(tAry)==true && tempB == true){
										console.log("   There are duplicate dragsources in one of your answer arrays: "+key2);
									}

								}else{
									console.log("   You are missing at least one object: 1");
								}


							}

							if(slides[key][slides[key].type].dropCounter !=undefined){
								if(Object.keys(slides[key][slides[key].type].quiz).length != Object.keys(slides[key][slides[key].type].dropCounter).length){
									console.log("   The numbers of dropsources in the quiz doesn't match with the numbers of dropsources in the dropCounter.");
								}else{
									var tMaxLength = 0;
									for(key2 in slides[key][slides[key].type].quiz){
										if(!($("#"+key2).length)){
											console.log("   The name of the dropsource wasn't found in the index.html: "+key2);
										}
										if(slides[key][slides[key].type]["dropCounter"][key2] == undefined){
											console.log("   The dropsources in the quiz doesn't match with the dropsources in the dropCounter: "+ key2);		
										}
										if(Object.keys(slides[key][slides[key].type]["quiz"][key2]).length > 0){
											// console.log(slides[key][slides[key].type]["quiz"][key2]["1"].length);
											tMaxLength = slides[key][slides[key].type]["quiz"][key2]["1"].length;
											if(tMaxLength > slides[key][slides[key].type]["dropCounter"][key2]){
												console.log("   Your length of your answerarray is longer than the corresponding numbers of dropsources in dropCounter.");
											}
										}

										// if(slides[key][slides[key].type]["quiz"][key2]["1"])
										
									}
								}

							}else{
									console.log("   You are missing an object: dropCounter");
							}
						}else{					
							console.log("   You are missing your objects: 'dragsources'");
						}
					}else{
						console.log("   You are missing an object: quiz");
					}

					if(slides[key][slides[key].type].actions !=undefined){					
						removeDuplicatesArray(tAry);
						for(var i=0;i<tAry.length;i++){
							// console.log(tAry[i]);
							
							if(slides[key][slides[key].type]["actions"][tAry[i]] == undefined){
								console.log("   You are missing your drag element in actions: " +tAry[i]);							
							}else{
								if(slides[key][slides[key].type]["actions"][tAry[i]]["onDragStart"]==undefined){
									console.log("   Warning! There is no onDragStart for "+ tAry[i] +" , but this might be intended!");
								}
								if(slides[key][slides[key].type]["actions"][tAry[i]]["onDrag"]==undefined){
									console.log("   Warning! There is no onDrag for "+ tAry[i] +" , but this might be intended!");
								}
								if(slides[key][slides[key].type]["actions"][tAry[i]]["onDragOver"]==undefined){
									console.log("   Warning! There is no onDragOver for "+ tAry[i] +" , but this might be intended!");
								}else{
									for(key2 in slides[key][slides[key].type]["actions"][tAry[i]]["onDragOver"]){
										// console.log(key2);
										// console.log(Object.keys(slides[key][slides[key].type]["quiz"]));
										if(Object.keys(slides[key][slides[key].type]["quiz"]).indexOf(key2) == -1){
											console.log("   The key "+ key2 +" isn't a drag element. Typo inside onDragOver.");
										}
									}
								}
								if(slides[key][slides[key].type]["actions"][tAry[i]]["onDragEnd"]==undefined){
									console.log("   Warning! There is no onDragEnd for "+ tAry[i] +" , but this might be intended!");
								}
								if(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"]==undefined){
									console.log("   Warning! There is no onDrop for "+ tAry[i] +" , but this might be intended!");
								}else{
									// console.log(tAry[i])
									// console.log(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"])
									if(Object.keys(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"])!=0){
										for(key2 in slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"]){
											// console.log(key2);
											// console.log(Object.keys(slides[key][slides[key].type]["quiz"]));
											
											// console.log(Object.keys(slides[key][slides[key].type]["quiz"]).equals(Object.keys(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"])));
											if(!Object.keys(slides[key][slides[key].type]["quiz"]).equals(Object.keys(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"]))){
												var x = arr_diff(Object.keys(slides[key][slides[key].type]["quiz"]),Object.keys(slides[key][slides[key].type]["actions"][tAry[i]]["onDrop"]));
												console.log("   Warning! There is no key "+x +" inside of "+ tAry[i]+" onDrop, but this might be intended!");
											}
										
										}
									}else{
										console.log("   Warning! There is an onDrop object, but there is nothing inside: "+ tAry[i]);

									}
								}
							}
						}										
					}else{
						console.log("   You are missing an object: actions");
					}

					if(slides[key][slides[key].type].trys == undefined){
						console.log("   You are missing a key: trys //dontedit");
					}else{
						if(slides[key][slides[key].type].trys != 0){
							console.log("   You have to set trys to 0. You have: " + slides[key][slides[key].type].trys );
						}
					}
					if(slides[key][slides[key].type].type == "dnd_1"){
						if(slides[key][slides[key].type].revertTime == undefined){
							console.log("   You are missing a key: revertTime");
						}
					}
					if(slides[key][slides[key].type].snapping == undefined){
						console.log("   You are missing a key: snapping");
					}else{
						if(typeof(slides[key][slides[key].type].snapping) != "boolean"){
							console.log("   Only the boolean true or false is allowed on snapping: "+ slides[key][slides[key].type].snapping);
						}
					}
					if(slides[key][slides[key].type].userAttempts == undefined){
						console.log("   You are missing a key: userAttempts");
					}else{					
						if(!(slides[key][slides[key].type].userAttempts >= 1 && slides[key][slides[key].type].userAttempts <= 2)){
							console.log("   You have a wrong value in userAttempts. It can only be 1 or 2. You have: " + slides[key][slides[key].type].userAttempts );
						}
					}
					if(slides[key][slides[key].type].type != undefined){
						if(!(slides[key][slides[key].type].type =="dnd_1" || slides[key][slides[key].type].type =="dnd_2" )){
							console.log("   You have the wrong type for a dnd(only dnd_1 or dnd_2): "+slides[key][slides[key].type].type);
						}
					}else{
						console.log("   You are missing a key: type");	
					}


				}//end dnd

				if(slides[key].type == "cq"){				
					if(slides[key].onChoiceQuestionAction == undefined){
						console.log("   You are missing an object: onChoiceQuestionAction");
					}
					if(slides[key][slides[key].type].trys == undefined){
						console.log("   You are missing a key: trys //dontedit");
					}else{
						if(slides[key][slides[key].type].trys != 0){
							console.log("   You have to set trys to 0. You have: " + slides[key][slides[key].type].trys );
						}
					}
					if(slides[key][slides[key].type].userAttempts == undefined){
						console.log("   You are missing a key: userAttempts");
					}else{					
						if(!(slides[key][slides[key].type].userAttempts >= 1 && slides[key][slides[key].type].userAttempts <= 2)){
							console.log("   You have a wrong value in userAttempts. It can only be 1 or 2. You have: " + slides[key][slides[key].type].userAttempts );
						}
					}
				}//end cq


				if(slides[key].onSuccessAction == undefined){
					console.log("   You are missing an object: onSuccessAction");
				}
				if(slides[key].onFailureAction1 == undefined){
					console.log("   You are missing an object: onFailureAction1");
				}
				if(slides[key].onFailureAction2 == undefined){
					console.log("   You are missing an object: onFailureAction2");
				}
			}

			if(slides[key].type != "video"){
				if(slides[key].audio == undefined){
					console.log("   You are missing an object: audio (VO for slide/feedbacks)");
				}else{
					if(Object.keys(slides[key].audio).length == 0){
						console.log("   Warning! There is no slide/feedback audio, but this might be intended!");
					}
				}
			}

			if(slides[key].type == "video"){
				if(slides[key].videoLoaded == undefined){
					console.log("   You are missing an object: videoLoaded//dontedit");
				}
			}


			if(slides[key].visited == undefined){
				console.log("   You are missing an object: visited //dontedit");
			}
		}else{
			console.log("   The name slide wasn't found in the index.html: "+key);
			
		} //end html check

		if(isQuiz(slides[key].type)){
			if(!($("#"+key).find("h1").length)){
				console.log("   There is no headline tag");
			}
			if(!($("#"+key).find(".FB_Nneg").length)){
				console.log("   There is no second negative feedback tag");
			}
			if(!($("#"+key).find(".FB_Neg").length)){
				console.log("   There is no first negative feedback tag");
			}
			if(!($("#"+key).find(".FB_Pos").length)){
				console.log("   There is no positive feedback tag");
			}
			if(slides[key].type=="scq" || slides[key].type =="mcq"){
				if(!($("#"+key).find("."+slides[key].type).length)){
					console.log("   The ul element witht the slide type class is missing: "+ "<ul class='"+slides[key].type+"'...");
				}
			}
			if(slides[key].type=="dnd"){
				if(!($("#"+key).find(".icon_size").length)){
					console.log("   There are no help icons on this slide.");
				}
				if(!($("#"+key).find(".si_Reset").length)){
					console.log("   There is no reset button/div on this slide.");
				}
				if(!($("#"+key).find(".si_Submit").length)){
					console.log("   There is no submit button/div on this slide.");
				}
				if(!($("#"+key).find(".draggable").length)){
					console.log("   There is no draggable element on this slide.");
				}
				if(!($("#"+key).find(".droppable").length)){
					console.log("   There is no droppable element on this slide.");
				}
			}
			globalVar.$curSlide.find(".draggable").each(function(){
				if(!(Object.keys(slides[globalVar.curSlide]["dnd"]["actions"]).indexOf(this.id) >= 0)){
						console.log("   There is a drag element in the index.html that isn't correct yet: "+this.id);
				}
		});	
		}

		var n=1;
		for(var o=0;o<snums.length;o++){		
			if(n != snums[o][0])
			console.log("You are missing a slidenumber in order of your slides: " + n );
			n++;
		}

	}//end check
}//end function

function hasDuplicates(array) {
    return (new Set(array)).size !== array.length;
}
function isQuiz(type){
  // var bol = false;
  if(type  == "mcq" || type  == "scq" || type  == "cq"  || type.indexOf("dnd")>=0){
    return true;
  }else{
    return false;
  }
}
// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

function cqButtons(s,type){	
	var $buttons = $(s);			
	// reset all the buttons on slide enter	
	$buttons.off("click");
	$buttons.data("checked", false);
	
	$(s).children().each(function(i,el){
		if(i%2 == 1){
			$(el).hide();
		}
		if(i%2 == 0){
			$(el).show();
		}
	});

	if(type == "mcq"){

		$buttons.on("mouseenter mouseleave click touch", function (event) {
			var $this = $(this);
			if (event.type === 'mouseenter' && $this.data("checked") === false) {
				$this.children().eq(0).hide();
				$this.children().eq(1).show();
			} else if (event.type === 'mouseleave' && $this.data("checked") === false) {
				$this.children().eq(0).show();
				$this.children().eq(1).hide();
			} else if (event.type === 'click' && $this.data("checked") === false) {
				$this.children().eq(0).hide();
				$this.children().eq(1).show();
				$this.data("checked", true);
				$(".icon_size").fadeOut();
				audPlayerMouseClick.play();
			} else if(event.type === 'click' && $this.data("checked") === true) {
				audPlayerMouseClick.play();
				$this.children().eq(0).show();
				$this.children().eq(1).hide();
				$this.data("checked", false);
				audPlayerMouseClick.play();
			}
		});	
	}else{

		$buttons.on("mouseenter mouseleave click touch", function (event) {
			var $this = $(this);
			// console.log($this.data("checked"));			
			if (event.type === 'mouseenter' && $this.data("checked") === false) {
				$this.children().eq(0).hide();
				$this.children().eq(1).show();
			} else if (event.type === 'mouseleave' && $this.data("checked") === false) {
				$this.children().eq(0).show();
				$this.children().eq(1).hide();
			} else if (event.type === 'click') {
				$buttons.each(function(){
					$(this).children().eq(0).show();
					$(this).children().eq(1).hide();					
					$(this).data("checked",false);
				});
				$this.children().eq(0).hide();
				$this.children().eq(1).show();
				$this.data("checked", true);
				$(".icon_size").fadeOut();
				audPlayerMouseClick.play();
			} 
		});	
	}
}

//Disables/Enables Controls, on Slide still visible (behind transparent), on Video not visible(behind video)
function disableControls() {
	$("#smar_GUI").removeClass("elz-15").addClass("elz--1");
}

function enableControls() {
	$("#smar_GUI").removeClass("elz--1").addClass("elz-15");
}
function Menu_Open() { //Function to open the menu overlay(blocker)
	$("#Menu_Overlay").fadeTo("slow", 1);
	$("#Menu_Overlay").css("zIndex", 9);
	$("#Menu_Overlay").css("display", "block");
}

function Menu_Close() { //Function to close the menu overlay(blocker)
	$("#Menu_Overlay").fadeTo("slow", 0);
	$("#Menu_Overlay").css("zIndex", -1);
	$("#Menu_Overlay").css("display", "none");
}

function TOC_Opener() { //Function to open and close the TOC
	$("#Menu_Side").toggleClass("helpboxshown helpboxhidden");
	if ($("#Menu_Side").hasClass("helpboxshown")) {
		Menu_Open();
	} else {
		Menu_Close();
	}
}
//Some shortcuts for functions
function nextSlide() {
	Reveal.next();
}

function prevSlide() {
	Reveal.prev();
}

function nextHide() {
	$("#smar_Next").hide();
}

function nextShow() {
	$("#smar_Next").show();
}

function backHide() {
	$("#smar_Back").hide();
}

function backShow() {
	$("#smar_Back").show();
}

function jumpToSlide(n) {
	Reveal.slide(n - 1);
}

function elementsFadeIn(el,x){
	setTimeout(function(){
		$(el).fadeTo("fast",1);
	},x);
}

function elementsFadeOut(el,x){
	setTimeout(function(){
		$(el).fadeTo("fast",0);
	},x);
}

function sortNumberInArray(a,b) {
	return a - b;
}

// ary.sort(sortArrayMatrixFunction); must be ary=[[4,xx],[2,xx],[1,xx]]
function sortArrayMatrixFunction(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
}
//Checks a string for nth occurence of a specfic character, returns position
function nth_occurrence(string, char, nth) { 
	var first_index = string.indexOf(char);
	var length_up_to_first_index = first_index + 1;

	if (nth == 1) {
		return first_index;
	} else {
		var string_after_first_occurrence = string.slice(length_up_to_first_index);
		var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

		if (next_occurrence === -1) {
			return -1;
		} else {
			return length_up_to_first_index + next_occurrence;
		}
	}
}

function findAndReplace(string, target, replacement) {
  var i = 0, length = string.length;
  for (i; i < length; i++) {
   string = string.replace(target, replacement);
 } 
 return string; 
}
//remove Duplicates in an Array
function removeDuplicatesArray(asd){//Function for remove duplicates
	return test = asd.filter(function(elem,index,self){
		return index == self.indexOf(elem);
	});
}

//shuffle an array, returns shuffled array
function shuffle(array) {
	var currentIndex = array.length,
		temporaryValue, randomIndex;
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}

//adds equals/checks to see if arrays equal each other
Array.prototype.equals = function (array) {
	// if the other array is a falsy value, return
	if (!array)
		return false;

	// compare lengths - can save a lot of time 
	if (this.length != array.length)
		return false;

	for (var i = 0, l = this.length; i < l; i++) {
		// Check if we have nested arrays
		if (this[i] instanceof Array && array[i] instanceof Array) {
			// recurse into the nested arrays
			if (!this[i].equals(array[i]))
				return false;
		} else if (this[i] != array[i]) {
			// Warning - two different object instances will never be equal: {x:20} != {x:20}
			return false;
		}
	}
	return true;
}

function hasDuplicates(array) {
	var valuesSoFar = [];
	for (var i = 0; i < array.length; ++i) {
			var value = array[i];
			if (valuesSoFar.indexOf(value) !== -1) {
					return true;
			}
			valuesSoFar.push(value);
	}
	return false;
}

function arr_diff (a1, a2) {

	var a = [], diff = [];

	for (var i = 0; i < a1.length; i++) {
			a[a1[i]] = true;
	}

	for (var i = 0; i < a2.length; i++) {
			if (a[a2[i]]) {
					delete a[a2[i]];
			} else {
					a[a2[i]] = true;
			}
	}

	for (var k in a) {
			diff.push(k);
	}

	return diff;
}

//execute whatever is inside onEnterAction, sets visited to true
function onEnterAction(id) {
	setTimeout(function () {
		if (slides[id].onEnterAction != "") {
			slides[id].onEnterAction();
		}
		slides[id].visited = true;
	}, 10);
}

function disableSubmit(){
	globalVar.$curSlide.find(".si_Submit").off("click touch");
}
function enableSubmit(){
	globalVar.$curSlide.find(".si_Submit").on("click touch", function () {
		audPlayerMouseClick.play();
		var bol = QCheck(slides[globalVar.curSlide].type);
		if (slides[globalVar.curSlide].type != "cq") {
			FBCheck(bol);
		}else{
			slides[globalVar.curSlide].onChoiceQuestionAction();
		}
	});
}

function restoreDataToCourse(){
	if (globalVar.scormSettings.useScorm) {
		setTimeout(function () {
			if (scorm.get("cmi.suspend_data").length > 0) {
				var ary = scorm.get("cmi.suspend_data").split(",");
				var x = recieveDataFromLMS().split(",");
				for(var i = 0; i<x.length;i++){					
					window[x[i]] = parseInt(ary[i]);					
				}
				var n = 1;
				for(var i = x.length; i<ary.length;i++){
					window["C_Q"+n] = parseInt(ary[i]);
					n++;		
				}
				setTimeout(function(){
					slides[globalVar.curSlide].onEnterAction();
				},55);
				console.log("something is stored //oEA called");
			} else {
				console.log("nothing is stored");
			}
		}, 800);

		
		setTimeout(function(){
			if(globalVar.scormSettings.bookmarking.useBookmarking && globalVar.scormSettings.useScorm){
				window[globalVar.scormSettings.bookmarking.name] = scorm.get("cmi.core.lesson_location");
				jumpToSlide(window[globalVar.scormSettings.bookmarking.name]);
			}
		},255);
	}
}

//Some control functions that will not change (Elements that needed
//the document ready function)
$(document).ready(function () {
	/*---------INIT--------*/
	globalVar.curSlide = $("section" + ".present")[0].id;

	$("aside").remove();
	
	//if idbehold is true, start cheating/execute code inside idbehold();
	if(globalVar.idbehold == true){
		idbehold();
		Reveal.configure({history:true});
	}else{
		Reveal.configure({history:false});
	}

	for(key in slides){
		if(slides[key].type =="video"){
			globalVar.videoSeen[key] = false;
		}
	}

	if($("#Screen_Chart").length != 0){
		chartInitiate();
	}

	//gets all chapter variables
	for(var i = 0;i<=1000;i++){
		if(window["C_Q"+i] != undefined){
		chap_nums++;
		window["C_Q"+i+"_done"] = false;
		}
	}

	functionCalls();

	firstTimeSetup();

	randomizeQuestionSlides();

	Reveal.addEventListener("slidechanged", function (e) {
		globalVar.curSlide = e.currentSlide.id;

		functionCalls();
	
	});

	setTimeout(function(){
		$("#scorm_whiteOverlay").fadeOut();
	},1000);

	//What happens when you click on a submit button
	$(".si_Submit").on("click touch", function () {
		audPlayerMouseClick.play();
		var bol = QCheck(slides[globalVar.curSlide].type);
		if (slides[globalVar.curSlide].type != "cq") {
			FBCheck(bol);
		}else{
			slides[globalVar.curSlide].onChoiceQuestionAction();
		}
	});




	$(".si_Button").hover(function () {
		var $this = $(this);
		$this.children().eq(1).show();
		$this.children().eq(0).hide();
	}, function () {
		var $this = $(this);
		$this.children().eq(0).show();
		$this.children().eq(1).hide();
		$this.children().eq(2).hide();
	});

	$(".si_Button").on("click touch", function () {
		audPlayerMouseClick.play();		
		var $this = $(this);
		$this.children().eq(0).hide();
		$this.children().eq(1).hide();
		$this.children().eq(2).show();
		setTimeout(function () {
			$this.children().eq(0).show();
			$this.children().eq(1).hide();
			$this.children().eq(2).hide();
			setTimeout(function () {
				Reveal.slide($this.data("slide"));
			}, 35);
		}, 55);
	});

	$("#smar_Back").on("click touch", function () {
		audPlayerMouseClick.play();
		setTimeout(function () {
			if (slides[globalVar.curSlide].backAction != "") {
				slides[globalVar.curSlide].backAction();
			} else {
				prevSlide();
			}
		}, 100);
	});

	$("#smar_Next").on("click touch", function () {
		audPlayerMouseClick.play();
		setTimeout(function () {
			if (slides[globalVar.curSlide].nextAction != "") {
				slides[globalVar.curSlide].nextAction();
			} else {
				nextSlide();
			}
		}, 100);
	});

	$("#smar_Replay").on("click touch", function () {
		audPlayerMouseClick.play();
		jumpToSlide(0);
	});
 
	$("#smar_Exit").on("click touch", function () {
		audPlayerMouseClick.play();
		window.close();
	});

	$("#smar_Play").on("click touch", function () {
		audPlayerMouseClick.play();
		if($("#smar_sub_Pause").css("display").indexOf("block") >= 0){
			audPlayerOne.pause();			
			$("#smar_sub_Play").show();
			$("#smar_sub_Pause").hide();
			audUser = true;
		}else{
			audPlayerOne.play();
			$("#smar_sub_Play").hide();
			$("#smar_sub_Pause").show();
			audUser = false;
		}
	});

	$("#Info_Closer").on("click touch", function () {
		globalVar.$curSlide.children().removeClass('cursorDisabled');
		audPlayerMouseClick.play();
		$("#Info_Overlay").fadeOut();
		enableControls();
	});

	$("#smar_Home").on("click touch", function () {
		audPlayerMouseClick.play();
		jumpToSlide(globalVar.homeSlide);
	});

	$("#smar_Info").on("click touch", function () {
		globalVar.$curSlide.children().addClass('cursorDisabled');
		audPlayerMouseClick.play();
		$("#Info_Overlay").fadeIn();
		disableControls();
	});

	$("#smar_Menu").on("click touch", function () {
		audPlayerMouseClick.play();
		TOC_Opener();
	});

	$("#Menu_Overlay").on("click touch", function () {
		audPlayerMouseClick.play();
		TOC_Opener();
	});

	//set font sizes
	var fontSize = $('.slides').width() / globalVar.fontSizeFactor;
	$("html").css("font-size",fontSize);
	

	$(window).resize(function () {
		var fontSize = $('.slides').width() / globalVar.fontSizeFactor;
		$("html").css("font-size",fontSize);
	});

	$("img").on("dragstart", function (event) {
		event.preventDefault();
	});

});