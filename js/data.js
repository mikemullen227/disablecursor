$(document).ready(function () {
	slides = {
		Start: {
			slideNumber: 1,
			type: 'text',
			navElements : [],
			backAction: "",
			nextAction: "",
			onEnterAction: function () {
				//stuff
				$("#progress-bar").hide();
				curChapter = 1;
			},
			audio: {
				// onEnter: "media/audio/Start/0.mp3"
			},
			visited: false //dontedit
		},
		LO: {
			slideNumber: 2,
			type: 'text',
			navElements : ["Back","Next","Home","Info","Menu","Chart","Play"],
			backAction: prevSlide,
			nextAction: nextSlide,
			onEnterAction: function () {
				curChapter = 2;
				$("#progress-bar").show();
				$(".dott").eq(0).css("background-color","#ff9900");
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#9d9d9c");
				$(".dott").eq(3).css("background-color","#9d9d9c");
				$(".dott").eq(4).css("background-color","#9d9d9c");
				$(".dott").eq(5).css("background-color","#9d9d9c");
				$(".dott").eq(6).css("background-color","#9d9d9c");
				$(".dott").eq(7).css("background-color","#9d9d9c");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");
			},
			audio: {
				// onEnter: "media/audio/LO/0.mp3"
			},
			visited: false //dontedit
		},
		V1: {
			slideNumber: 3,
			type: 'video',
			navElements : ["Back","Next"],
			backAction: prevSlide,
			nextAction: nextSlide,
			onEnterAction: function(){
				// stuff
				curChapter = 3;
			},
			videoSeeking : true,
			videoLoaded: false, //dontedit
			visited: false //dontedit
		},
		Q1: {
			slideNumber: 4,
			type: 'mcq',
			navElements : ["Back","Next","Home","Info","Menu","Chart","Play"],
			backAction: prevSlide,
			nextAction: function(){
				nextSlide();
			},
			onEnterAction: function () {
				// stuff
				// backHide();
				// nextHide();
				curChapter = 4;
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#9d9d9c");
				$(".dott").eq(5).css("background-color","#9d9d9c");
				$(".dott").eq(6).css("background-color","#9d9d9c");
				$(".dott").eq(7).css("background-color","#9d9d9c");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");
			},
			mcq: {
				quiz: {
					1:{
						"Make learning success measurable": "1",
						"Query knowledge playfully": "1",
						"Allow self-examination": "1"
					}
				},
				randomize: true,
				audio: {
					// "Make learning success measurable": "media/audio/Q1/1.mp3",
					// "Query knowledge playfully": "media/audio/Q1/2.mp3",
					// "Allow self-examination": "media/audio/Q1/3.mp3"
				},
				userAttempts: 2,
				trys: 0 //dontedit
			},
			onSuccessAction: function () {
				if(C_Q1 != 1){
					correctAnswers++;
				}
				C_Q1 = 1;
			},
			onFailureAction1: "",
			onFailureAction2: "",
			audio: {
				// onEnter: "media/audio/Q1/0.mp3",
				// FBPos: "media/audio/Q1/FBp.mp3",
				// FBNeg: "media/audio/Q1/FBn.mp3",
				// FBNneg: "media/audio/Q1/FBnn.mp3"
			},
			visited: false //dontedit
		},
		Q2: {
			slideNumber: 5,
			type: 'scq',
			navElements : ["Back","Next","Home","Info","Menu","Chart","Play"],
			backAction: prevSlide,
			nextAction: function(){
				nextSlide();
			},
			onEnterAction: function () {
				// stuff
				// backHide();
				// nextHide();
				curChapter = 5;
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#ff9900");
				$(".dott").eq(5).css("background-color","#9d9d9c");
				$(".dott").eq(6).css("background-color","#9d9d9c");
				$(".dott").eq(7).css("background-color","#9d9d9c");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");
			},
			scq: {
				quiz: {
					1:{
						"Make learning success measurable": "1",
						"Query knowledge playfully": "0",
						"Allow self-examination": "0"
					}
				},
				randomize: true,
				audio: {
					// "Answer 1": "media/audio/Q1/1.mp3",
					// "Answer 2": "media/audio/Q1/2.mp3",
					// "Answer 3": "media/audio/Q1/3.mp3"
				},
				userAttempts: 1,
				trys: 0 //dontedit
			},
			onSuccessAction: function () {
				if(C_Q2 != 1){
					correctAnswers++;
				}
				C_Q2 = 1;
			},
			onFailureAction1: "",
			onFailureAction2: "",
			audio: {
				// onEnter: "media/audio/Q1/0.mp3",
				// FBPos: "media/audio/Q1/FBp.mp3",
				// FBNeg: "media/audio/Q1/FBn.mp3",
				// FBNneg: "media/audio/Q1/FBnn.mp3"
			},
			visited: false //dontedit
		},
		Q3: {
			slideNumber: 6,
			type: 'dnd',
			navElements : ["Back","Next","Home","Info","Menu","Chart","Play"],
			backAction: prevSlide,
			nextAction: nextSlide,
			onEnterAction: function () {
				// nextHide();
				// backHide();
				// stuff
				curChapter = 6;
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#ff9900");
				$(".dott").eq(5).css("background-color","#ff9900");
				$(".dott").eq(6).css("background-color","#9d9d9c");
				$(".dott").eq(7).css("background-color","#9d9d9c");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");
			},	
			dnd: {
				quiz: {
					drop1: {
						1:["drag1"]
						//if you want multiple answers here put a - n:["dragID"]
					},
					drop2: {
						1:["drag2"]
						//if you want multiple answers here put a - n:["dragID"]
					},
					drop3: {
						1:["drag3"]
						//if you want multiple answers here put a - n:["dragID"]
					}
				},
				dropCounter: {drop1: 1,drop2:1,drop3:1},
				actions:{
					drag1:{
						onDragStart : function(){},
						onDrag : function(){},
						onDragOver : {
							drop1:function(){}
						},
						onDragEnd : function(){},
						onDrop : {
							drop1:function(){}
						}
					},
					drag2:{
						onDragStart : function(){},
						onDrag : function(){},
						onDragOver : {
							drop1:function(){}
						},
						onDragEnd : function(){},
						onDrop : {
							drop1:function(){},
							drop2:function(){},

						}
					},
					drag3:{
						onDragStart : function(){},
						onDrag : function(){},
						onDragOver : {
							drop1:function(){}
						},
						onDragEnd : function(){},
						onDrop : {
							drop1:function(){}
						}
					}	
				},
				revertTime: 300,
				snapping: true, //only single drags to drops! (e.g. fill in the gaps)
				userAttempts: 2,
				trys: 0, //dontedit
				type: "dnd_1"
			},
			onSuccessAction: function () {
				if(C_Q3 != 1){
					correctAnswers++;
				}
				C_Q3 = 1;
			},
			onFailureAction1: "",
			onFailureAction2: "",
			audio: {
				// onEnter: "media/audio/Q2/0.mp3",
				// FBPos: "media/audio/Q2/FBp.mp3",
				// FBNeg: "media/audio/Q2/FBn.mp3",
				// FBNneg: "media/audio/Q2/FBnn.mp3"
			},
			visited: false //dontedit
		},
		Q4: {
			slideNumber: 7,
			type: "dnd",	
			navElements: ["Back","Next", "Home", "Info","Menu", "Chart", "Play"],
			backAction: prevSlide,
			nextAction: nextSlide,
			onEnterAction: function () {
				curChapter = 7;
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#ff9900");
				$(".dott").eq(5).css("background-color","#ff9900");
				$(".dott").eq(6).css("background-color","#ff9900");
				$(".dott").eq(7).css("background-color","#9d9d9c");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");
			},
			dnd: { //add this object if you have a drag and drop slide
				quiz: { //inside this object goes the answers with the id that are assigned inside the index.html
					LZ_4_Q1_drTarget1: 
					{
						1: ["LZ_4_Q1_drSource1", "LZ_4_Q1_drSource4"]
					}, 
					LZ_4_Q1_drTarget2:
					{
						1: ["LZ_4_Q1_drSource2", "LZ_4_Q1_drSource3"]				
					},
					LZ_4_Q1_drTarget3: 
					{
						1:["LZ_4_Q1_drSource5"]
					}
				},
				dropCounter : {
					LZ_4_Q1_drTarget1 : 2,
					LZ_4_Q1_drTarget2 : 2,
					LZ_4_Q1_drTarget3 : 1
				},
				actions:{
					LZ_4_Q1_drSource1:{
						onDragStart : function(){},
						// onDrag : function(){},
						// onDragOver : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// },
						// onDragEnd : function(){},
						// onDrop : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// }
					},
					LZ_4_Q1_drSource2:{
						// onDragStart : function(){},
						// onDrag : function(){},
						// onDragOver : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// },
						// onDragEnd : function(){},
						// onDrop : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// }
					},
					LZ_4_Q1_drSource3:{
						// onDragStart : function(){},
						// onDrag : function(){},
						// onDragOver : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// },
						// onDragEnd : function(){},
						// onDrop : {
						// 	LZ_4_Q1_drTarget1 : function(){},
						// 	LZ_4_Q1_drTarget2 : function(){},
						// 	LZ_4_Q1_drTarget3 : function(){}
						// }
					},			
					LZ_4_Q1_drSource4:{
						onDragStart : function(){},
						onDrag : function(){},
						onDragOver : {
							LZ_4_Q1_drTarget1 : function(){},
							LZ_4_Q1_drTarget2 : function(){},
							LZ_4_Q1_drTarget3 : function(){}
						},
						onDragEnd : function(){},
						onDrop : {
							LZ_4_Q1_drTarget1 : function(){},
							LZ_4_Q1_drTarget2 : function(){},
							LZ_4_Q1_drTarget3 : function(){}
						}
					},
					LZ_4_Q1_drSource5:{
						onDragStart : function(){},
						onDrag : function(){},
						onDragOver : {
							LZ_4_Q1_drTarget1 : function(){},
							LZ_4_Q1_drTarget2 : function(){},
							LZ_4_Q1_drTarget3 : function(){}
						},
						onDragEnd : function(){},
						onDrop : {
							LZ_4_Q1_drTarget1 : function(){},
							LZ_4_Q1_drTarget2 : function(){},
							LZ_4_Q1_drTarget3 : function(){}
						}
					}					
				},
				snapping: false,
				userAttempts: 2,
				trys: 0, //default to 0 (how many tries the user has made)
				type: "dnd_2" //this is dnd_type#1 - multiple drop targets and sources needs to get ordered correctly (more coming in future versions)
			},
			onSuccessAction: function () {
				if(C_Q4 != 1){
					correctAnswers++;
				}
				C_Q4 = 1;
			},
			onFailureAction1: "",
			onFailureAction2: "",
			audio: {
				// onEnter: "media/audio/LZ_4_Q1/0.mp3",
				// FBPos: "media/audio/LZ_4_Q1_FBp/0.mp3",
				// FBNeg: "media/audio/LZ_4_Q1_FBn/0.mp3",
				// FBNneg: "media/audio/LZ_4_Q1_FBnn/0.mp3"
			},
			visited: false //dontedit
		},
		Q5: {
			slideNumber: 8,
			type: "cq",
			navElements : ["Back","Next","Home","Info","Menu","Chart","Play"],
			nextAction: function(){
				nextSlide();
			},
			backAction: function(){
				prevSlide();	
			},			
			onEnterAction: function () {
				curChapter = 8;
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#ff9900");
				$(".dott").eq(5).css("background-color","#ff9900");
				$(".dott").eq(6).css("background-color","#ff9900");
				$(".dott").eq(7).css("background-color","#ff9900");
				$(".dott").eq(8).css("background-color","#9d9d9c");
				$(".dott").eq(9).css("background-color","#9d9d9c");

				cqButtons(".si_Button_2","scq"); //call a function that makes divs to a individual mcq (argument is a class and scq/mcq)
			},
			onChoiceQuestionAction: function (){ //!important this will get executed if you have an "cq" type, write here your query for checking correct/incorrect
				if ($("#LZ_1_Q1_Btn_1").data("checked") == true &&
					$("#LZ_1_Q1_Btn_2").data("checked") == true &&
					$("#LZ_1_Q1_Btn_3").data("checked") == true &&
					$("#LZ_1_Q1_Btn_4").data("checked") == true) {
					FBCheck(true);
				} else {
					FBCheck(false);
				}
			},
			onSuccessAction: function () {
				if(C_Q5 != 1){
					correctAnswers++;
				}
				C_Q5 = 1;
			},
			onFailureAction1: "", //the onFailureAction1 will get executed everytime the user answers the quiz incorrect for the first time (if blank nothing will happens except the standard)
		 	onFailureAction2: "",//the onFailureAction2 will get executed everytime the user answers the quiz incorrect for the second time (if blank, nothing will happens)
			cq:{
				trys:0, //dontedit
				userAttempts: 2
			},
			audio: {
				//  onEnter: "media/audio/LZ_1_Q1/0.mp3",
				//  FBPos: "media/audio/LZ_1_Q1_FBp/0.mp3",
				//  FBNeg: "media/audio/LZ_1_Q1_FBn/0.mp3",
				//  FBNneg: "media/audio/LZ_1_Q1_FBnn/0.mp3"
			},
			visited: false
		},
		"End": {
			slideNumber: 9,
			type: 'text',
			navElements : ["Replay","Exit","Home","Info","Menu","Chart","Play"],
			backAction: prevSlide,
			nextAction: function(){},
			onEnterAction: function () {
				curChapter = 9;
				if(globalVar.scormSettings.useScorm){					
					scorm.set("cmi.core.lesson_status", "completed");
					scorm.set("cmi.core.score.raw","100");
				}
				
				$(".dott").eq(1).css("background-color","#ff9900");
				$(".dott").eq(2).css("background-color","#ff9900");
				$(".dott").eq(3).css("background-color","#ff9900");
				$(".dott").eq(4).css("background-color","#ff9900");
				$(".dott").eq(5).css("background-color","#ff9900");
				$(".dott").eq(6).css("background-color","#ff9900");
				$(".dott").eq(7).css("background-color","#ff9900");
				$(".dott").eq(8).css("background-color","#ff9900");
				$(".dott").eq(9).css("background-color","#9d9d9c");
				// stuff
				$("#FAIL").hide();		
				$("#WIN").hide();
				$("#WIN2").hide();
				$("#WIN3").hide();
				$("#WIN4").hide();
				$("#WIN5").hide();

				console.log(correctAnswers);
				
				if(correctAnswers == 5){
					// console.log("5");				
					$("#WIN5").show();
				}
				if(correctAnswers == 4){
					// console.log("4");			
					$("#WIN4").show();
				}
				if(correctAnswers == 3){
				// console.log("3");	
				$("#WIN3").show();
				}
				if(correctAnswers == 2){
					// console.log("2");			
					$("#WIN2").show();	
				}
				if(correctAnswers == 1){
					// console.log("1");		
					$("#WIN").show();	
				}
				if(correctAnswers == 0){
					// console.log("0");			
					$("#FAIL").show();
				}
			},
			audio: {
				// onEnter: "media/audio/Result/0.mp3",
			},
			visited: false //dontedit
		}
	}



});